============
new protocol
============

haalchemy is going to be rewritten on top of the  
zkcluster library I developed for connmon.  Additionally,
it will move from being a replacement for HAProxy into
a full "connection orchestration" tool.

1. The plugin uses SQLA 1.1 hooks exclusively.

2. The plugin will outright replace QueuePool with a new pool.

3. the new pool is still queue based, but links directly
   to the haalchemy service cluster.  

4. The pool upon startup (or first use?) initiates 
   an haalchemy client object.  this object runs its own 
   event loop, and also has to be present
   per-process, so the existing system of bouncing out new 
   state per-process will be used here.

5. For each haalchemy client:

   a. first request:  authentication

   b. second request: what application are we looking for.
      what process id / hostname / ppid, etc.

   c. server sends back a list of GUID / SQLA URL / stayconnected
      tuples

   d. local pool prepares that many _ConnectionRecord objects
      for use, puts them in Queue.Queue, and that's it; they 
      are persistent until the server changes the plan.

   e. _ConnectionRecord w/ stayconnected stays connected after
      checkin; without stayconnected, close() is called but
      we hold onto the message.

6. Full protocol, server to client:


    a. ADD <guid> <sqla_url> <stayconnected> - add a _ConnectionRecord

    b. REMOVE <guid> - remove a _ConnectionRecord

    c. DISCONNECT <guid> - disconnect a _ConnectionRecord and flip
       off its stayconnected flag

7. Protocol, client to server is similar to the connmon connect / 
   checkout / checkin / etc. events.  We will keep a connmon-style
   RemotePid collection around and still have that do largely
   the same thing; however it also has to include the GUIDs and 
   all the other info the server uses, to accomodate the use case
   of totally stopping and restarting the HAAlchemy service without 
   stopping the consuming clients.

8. The server:

   a. sends clients their initial list of connection records.  this is
      based on a configured default connection / overflow count, e.g. 
      the 5 / 10 model

   b. the server keeps 5 / 10 going for a client but adjusts it based
      on the client's use.  A client that hits the 15 mark (pooled + overflow)
      will have more guids sent over to it.  A client that is never hitting
      more than two conections will have guids removed from it.  A client
      that has connections idle for a few minutes will have them slowly
      removed.   This is how the system will actively close out idle
      connections from an idle application.

   c. the server keeps track of the total pool of guids that have been used
      or are in use by all clients , as well as which ones are using which.
      guids that sit on the server unconsumed for some period of time 
      (or immediately?) will be retired completely from memory.

   d. the total pool of guids can grow up to a percentage of the "max connections"
      of each database server overall.  Though here is where it gets crazy with
      the use of multiple database hosts, multiple applications within a database
      that need to have distinct connections, etc.  

   e. an exhausted use situation occurs when the server has handed out 
      guids up to a percentage of the total connections that can be used 
      by a host.   at this high mark it won't hand out more connection guids.
      It would have to wait for guids to become available again by them being
      pulled back from clients.



8. Advantages:

   a. still have the lightweight "send an event on checkin / checkout but
      don't actually wait for the server to approve us" model; clients
      have a pre-populated list of what they can connect to and don't need
      to wait if the server is lagging.

   b. we can keep a total collection of connection record GUIDs across
      all clients in the server state, but also it can be built up from 
      nothing if the server is restarted

   c. clients can still serve connections when the haalchemy server is
      down (though at the moment we're saying the server has to be running
      for it to start, though even that could be relaxed)

   d. the whole "which host do we connect to" thing is totally done on the
      server.  Clients just get a list of ADD commands and don't have to 
      do anything

   e. the server serves the whole SQLAlchemy URL to the client.  username/password/etc.
      don't need to be configured by clients at all.   using SSL certs to connect
      we have a good part of the "DB passwords not stored on the client machine"
      system going

   f. The full connmon-style functionality will be built into the same platform.
