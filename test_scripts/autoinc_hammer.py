"""
To use this script:

1. Create a Python virtualenv:

    virtualenv .venv

2. Install SQLAlchemy and MySQL-Python in the virtualenv:

    .venv/bin/pip install SQLAlchemy MySQL-Python

3. Run the script as follows:

    .venv/bin/python autoinc_hammer.py -u <username> -H <hostname> -p <password>

"""

from sqlalchemy import Table, MetaData, Column, create_engine, \
    Integer, String, select
import sqlalchemy as sa
from sqlalchemy.engine import url
import argparse
import random
import multiprocessing
import time

meta = MetaData()

for num in ("one", "two", "three", "four", "five"):
    Table(
        'data_table_%s' % num, meta,
        Column('id', Integer, primary_key=True, autoincrement=True),
        Column('data', String(50), nullable=False),
        mysql_engine='InnoDB'
    )

engine = None


def hammer(modulus, total, delay):
    # each process runs with a "modulus" so that
    # no two procs deal with the same set of primary keys
    engine.dispose()

    print("hammer starting with modulus: %s" % modulus)

    host = "<unknown>"

    def status(msg):
        print("%s Effective host %s modulus %s %s" % (time.time(), host, modulus, msg))

    proceed_cautiously = False

    while True:
        try:
            if proceed_cautiously and delay:
                status("proceed_cautiously is set (1), sleeping for %d" % delay)
                time.sleep(delay)

            with engine.begin() as conn:

                new_host = conn.execute(
                    "SHOW VARIABLES WHERE Variable_name = 'hostname'"
                ).first()[1]
                if host == "<unknown>":
                    status("Selected new host %s" % new_host)
                elif host != new_host:
                    status("Host switched from %s to %s" % (host, new_host))

                host = new_host

            if proceed_cautiously and delay:
                status("proceed_cautiously is set (2), sleeping for %d" % delay)
                time.sleep(delay)

            num = 0
            with engine.begin() as conn:
                for tab in range(10):
                    data_table = random.choice(meta.tables.values())

                    for id_ in range(5):
                        conn.execute(
                            data_table.insert(),
                            {"data": "some value %d" % id_}
                        )
                        num += 1
            status(
                "Inserted %s rows" % num)
            proceed_cautiously = False
            time.sleep(random.random() * 3)

        except sa.exc.IntegrityError as er1:
            status("Error!")
            print(er1)
            host = "<unknown>"
            time.sleep(2)
            proceed_cautiously = True
        except sa.exc.DBAPIError as er2:
            status("Error!")
            host = "<unknown>"
            print(er2)
            proceed_cautiously = True


def run():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-u", "--user", type=str, default="root", help="database username")
    parser.add_argument(
        "-p", "--password", type=str, help="database password")
    parser.add_argument(
        "-H", "--host", type=str, default="localhost", help="hostname"
    )
    parser.add_argument(
        "-n", "--num", type=int, default=20, help="number of processes"
    )
    parser.add_argument(
        "-P", "--port", type=int, help="port"
    )
    parser.add_argument(
        "-d", "--delay", type=float, default=0,
        help="delay before making a connection"
    )
    args = parser.parse_args()

    url_obj = url.URL(
        "mysql+mysqldb", username=args.user, password=args.password,
        host=args.host, port=args.port
    )

    eng = create_engine(url_obj)
    eng.execute("create database if not exists hammer")
    eng.dispose()

    url_obj.database = 'hammer'
    global engine
    engine = create_engine(url_obj, pool_recycle=5, isolation_level="READ_COMMITTED")
    meta.drop_all(engine)
    meta.create_all(engine)

    procs = []
    for i in range(args.num):
        proc = multiprocessing.Process(
            target=hammer, args=(i, args.num, args.delay))
        proc.daemon = True
        procs.append(proc)
        proc.start()
    while True:
        time.sleep(5)

if __name__ == '__main__':
    run()
