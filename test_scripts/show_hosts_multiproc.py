from sqlalchemy import create_engine
import sqlalchemy as sa
import argparse
import multiprocessing
import time
import random

engine = None


def hammer(modulus):

    host = "<unknown>"

    def status(msg):
        print(
            "%s Effective host %s modulus %s %s" %
            (time.time(), host, modulus, msg))

    while True:
        try:

            with engine.begin() as conn:

                result = conn.execute(
                    "SHOW VARIABLES WHERE Variable_name = 'hostname' "
                    "OR variable_name = 'port'"
                )
                info = dict(
                    (row['Variable_name'], row['Value'])
                    for row in result
                )
                new_host = "%s:%s" % (info['hostname'], info['port'])

                if host == "<unknown>":
                    status("Selected new host %s" % new_host)
                elif host != new_host:
                    status("Host switched from %s to %s" % (host, new_host))

                host = new_host

            time.sleep(random.random())
        except sa.exc.DBAPIError as er2:
            status("Error!")
            host = "<unknown>"
            print(er2)


def run():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-n", "--num", type=int, default=20, help="number of processes"
    )
    args = parser.parse_args()

    global engine

    import logging.config
    logging.config.fileConfig("sample.cfg")

    engine = create_engine(
        "haalchemy://A:X@rhel-cluster/?ha_cfg=sample.cfg")

    # engine = create_engine("haalchemy://@rhel-cluster", pool_recycle=60)
    # engine = create_engine("haalchemy://A:X@localhost:5084/rhel-cluster")
    engine.connect().close()

    procs = []
    for i in range(args.num):
        proc = multiprocessing.Process(
            target=hammer, args=(i, ))
        proc.daemon = True
        procs.append(proc)
        proc.start()

    while True:
        time.sleep(5)

if __name__ == '__main__':
    run()
