"""
To use this script:

1. Create a Python virtualenv:

    virtualenv .venv

2. Install SQLAlchemy and MySQL-Python in the virtualenv:

    .venv/bin/pip install SQLAlchemy MySQL-Python

3. Run the script as follows:

    .venv/bin/python run_hammer.py -u <username> -H <hostname> -p <password>

"""

from sqlalchemy import Table, MetaData, Column, create_engine, \
    Integer, String, select
import sqlalchemy as sa
from sqlalchemy.engine import url
import argparse
import random
import multiprocessing
import time

meta = MetaData()
data_table = Table(
    'data_table', meta,
    Column('id', Integer, primary_key=True, autoincrement=False),
    # the index will add some heft to inserts
    Column('data', String(50), nullable=False, index=True),
    mysql_engine='InnoDB'
)

engine = None


def hammer(modulus, total, delay):
    # each process runs with a "modulus" so that
    # no two procs deal with the same set of primary keys
    engine.dispose()

    # if a delay is given, we want to serialize
    # connections between subprocesses.  so multiply
    # delay by modulus
    if delay:
        _creator = engine.pool._creator
        our_delay = delay * modulus

        def creator():
            status("Delay %s sec before connect..." % our_delay)
            time.sleep(our_delay)
            return _creator()
        engine.pool._creator = creator

    print("hammer starting with modulus: %s" % modulus)

    numbers = set([
        (i * total) + modulus for i in range(14000)
    ])

    host = "<unknown>"

    def status(msg):
        print("Effective host %s modulus %s %s" % (host, modulus, msg))

    while True:
        try:
            with engine.begin() as conn:

                host = conn.execute(
                    "SHOW VARIABLES WHERE Variable_name = 'hostname'"
                ).first()[1]

                num_range = set(random.sample(numbers, 8000))
                if num_range:
                    rows = conn.execute(
                        select([data_table.c.id]).where(
                            data_table.c.id.in_(num_range))
                    )
                    existing_ids = set(
                        r[0] for r in rows
                    )
                else:
                    existing_ids = set()

                non_existing_ids = num_range.difference(existing_ids)

                to_delete = set(
                    random.sample(existing_ids, min(4000, len(existing_ids)))
                )

                to_insert = set(
                    random.sample(to_delete, min(2000, len(to_delete)))
                ).union(
                    random.sample(
                        non_existing_ids, min(2000, len(non_existing_ids)))
                )

                if to_delete:
                    conn.execute(
                        data_table.delete().where(
                            data_table.c.id.in_(to_delete))
                    )
                status("Deleted %s rows" % len(to_delete))

                conn.execute(
                    data_table.insert(),
                    [
                        {"id": id_, "data": "some value %d" % id_}
                        for id_ in to_insert
                    ]
                )
                status("Inserted %s rows" % len(to_insert))

                time.sleep(random.random() * .5)
        except sa.exc.OperationalError as err:
            status("Error!")
            print(err)
            time.sleep(2)


def run():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-u", "--user", type=str, default="root", help="database username")
    parser.add_argument(
        "-p", "--password", type=str, help="database password")
    parser.add_argument(
        "-H", "--host", type=str, default="localhost", help="hostname"
    )
    parser.add_argument(
        "-n", "--num", type=int, default=10, help="number of processes"
    )
    parser.add_argument(
        "-d", "--delay", type=float, default=0,
        help="delay before making a connection"
    )
    args = parser.parse_args()

    url_obj = url.URL(
        "mysql+mysqldb", username=args.user, password=args.password,
        host=args.host
    )

    eng = create_engine(url_obj)
    eng.execute("create database if not exists hammer")
    eng.dispose()

    url_obj.database = 'hammer'
    global engine
    from balancer import impl
    engine = impl.create_engine(url_obj, pool_recycle=15, isolation_level="READ_COMMITTED")
    meta.drop_all(engine)
    meta.create_all(engine)

    procs = []
    for i in range(args.num):
        proc = multiprocessing.Process(
            target=hammer, args=(i, args.num, args.delay))
        proc.daemon = True
        procs.append(proc)
        proc.start()
    while True:
        time.sleep(5)

if __name__ == '__main__':
    run()
