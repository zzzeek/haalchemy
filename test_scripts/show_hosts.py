from sqlalchemy import create_engine
import threading
import sqlalchemy as sa
import argparse
import time
import random
from haalchemy import frontend

engine = None


def hammer(modulus, threaded):
    if threaded:
        engine.dispose()

    host = "<unknown>"

    def status(msg):
        print(
            "%s Effective host %s modulus %s %s" %
            (time.time(), host, modulus, msg))

    start = time.time()
    while True:
        try:

            with engine.begin() as conn:

                result = conn.execute(
                    "SHOW VARIABLES WHERE Variable_name = 'hostname' "
                    "OR variable_name = 'port'"
                )
                info = dict(
                    (row['Variable_name'], row['Value'])
                    for row in result
                )
                new_host = "%s:%s" % (info['hostname'], info['port'])

                if host == "<unknown>":
                    status("Selected new host %s" % new_host)
                elif host != new_host:
                    status("Host switched from %s to %s" % (host, new_host))

                host = new_host

            if not threaded:
                if time.time() - start > 10:
                    frontend._log_status()
                    start = time.time()

            time.sleep(random.random())
        except sa.exc.DBAPIError as er2:
            status("Error!")
            host = "<unknown>"
            print(er2)


def run():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-n", "--num", type=int, default=1,
        help="number of threads, set to 1 for no threads"
    )
    args = parser.parse_args()

    global engine

    import logging.config
    logging.config.fileConfig("sample.cfg")

    engine = create_engine(
        "haalchemy://A:X@rhel-cluster/?ha_cfg=sample.cfg",
        pool_recycle=60)

    if args.num == 1:
        hammer(1, False)
    else:
        engine.connect().close()
        procs = []
        for i in range(args.num):
            proc = threading.Thread(
                target=hammer, args=(i, True))
            proc.daemon = True
            procs.append(proc)
            proc.start()

        frontend._log_stats_periodically()

        # import gc
        while True:
            time.sleep(15)
            # gc.collect()

if __name__ == '__main__':
    run()
