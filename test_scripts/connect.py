from sqlalchemy import create_engine

e = create_engine("mysql+pymysql://scott:tiger@local-cluster/test?plugin=haalchemy&ha_cfg=sample.cfg")

#e = create_engine("haalchemy://A:X@rhel-cluster/?ha_service_node1=localhost:5084&ha_service_node2=localhost:5085&appname=neutron")

#e = create_engine("mysql+haalchemy://rhel-cluster/?ha_service_node1=localhost:5084&ha_service_node2=localhost:5085&appname=neutron")

#e = create_engine(
#    "haalchemy://@rhel-cluster/"
#    "?haa_cfg=/path/to/file.cfg&start=true#neutron")

#e = create_engine("haalchemy://A:X@localhost:5084/rhel-cluster")

#e = create_engine(
#    "haalchemy://A:X@192.168.1.25:3084/rhel-cluster/hammer#neutron")

c1 = e.connect()
print c1.execute("SHOW VARIABLES WHERE Variable_name = 'hostname'").fetchall()
c1.close()


c1 = e.connect()


import pdb
pdb.set_trace()
