from setuptools import setup
from setuptools import find_packages
import os
import re


with open(
    os.path.join(os.path.dirname(__file__), 'haalchemy', '__init__.py')
) as file_:
    VERSION = re.compile(
        r".*__version__ = '(.*?)'", re.S).match(file_.read()).group(1)


readme = os.path.join(os.path.dirname(__file__), 'README.rst')

requires = [
    'SQLAlchemy', # >=1.1',
    'zkcluster',
]

setup(
    name='haalchemy',
    version=VERSION,
    description="Load balancing for SQLAlchemy connection pools",
    long_description=open(readme).read(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Database :: Front-Ends',
    ],
    keywords='SQLAlchemy HA',
    author='Mike Bayer',
    author_email='mike@zzzcomputing.com',
    url='http://bitbucket.org/zzzeek/haalchemy',
    license='MIT',
    packages=find_packages('.', exclude=['examples*', 'test*']),
    include_package_data=True,
    tests_require=['mock'],
    zip_safe=False,
    install_requires=requires,
    entry_points={
        'console_scripts': ['ha-agent = haalchemy.cmd:main'],
        'sqlalchemy.plugins': [
            'haalchemy = haalchemy.clients.sqlalchemy.plugin:HAPlugin',
        ]
    }
)
