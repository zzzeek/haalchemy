=========
HAAlchemy
=========

HAAlchemy is a project under development by Mike Bayer, creator of
SQLAlchemy, which will attempt to provide in-application load-balancing
and failover for database clusters.    The key concept is that a single
Engine / connection pool will be able to directly connect to multiple
servers.

For docs in progress, see `HAAlchemy <http://haalchemy.readthedocs.org>`_ on readthdocs.