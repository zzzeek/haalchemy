=====================================
Welcome to HAAlchemy's documentation!
=====================================

.. toctree::
    :hidden:

    self

.. note::

    HAAlchemy is under development.  While most of the individual  components
    of its functionality have already been built, either here or as part of the
    "connmon" application, these components are in the process of being re-
    organized into a single deployment architecture with more capbilities than
    the first iteration of HAAlchemy/connmon, which focused only on database
    routing and connection pool monitoring respectively. The documentation
    below is currently a "blueprint" for what the finished product should look
    like.

`HAAlchemy <http://bitbucket.org/zzzeek/haalchemy>`_ is a database connection
orchestrator for multi-process / multi-host / multi-database applications.
Oriented towards Python / `SQLAlchemy <http://www.sqlalchemy.org>`_
applications, it can in theory be used with non-Python applications as well.

Overview
========

HAAlchemy runs as a Python service which interacts with clients embedded within
consuming applications. The service sends configuration directives to the
client which govern the client application's database connectivity behaviors
and limits.  The directives include details as to what database host or hosts
should be in use as well as  how many connections should be persistently pooled
vs. released on each use.   The client, in turn, acts as a local factory
for database connections on behalf of its application, passing events
back to the server indicating all connection activity, including connections
and disconnections, checkouts and checkins of pooled connections, database
errors, and invalidations.  The server sends adjustments to clients based
on real time analysis of database use across all applications, continuously
adding and removing potential and actual database connection slots in response
to the individual demand of each application.

The design leverages the ability to embed part of HAAlchemy's functionality
within consuming applications themselves, in order to provide a novel alternative
to the more traditional approach of using proxy servers for load balancing,
failover, and full-cluster connection pooling.   Whereas network-resource-governing
middleware typically places itself directly in the path of communication
between the ultimate client and service, HAAlchemy stays off to the side,
allowing direct communication between clients and servers, but sending
governing instructions in real time in order to affect the
overall and per-application usage of database resources.

.. figure:: haalchemy.png
    :alt: HAAlchemy Cluster

The primary goals of HAAlchemy are:

* **Virtual Connection Pooling** - Applications connect to databases
  based on a local set of available slots received from the server, and
  block when no more slots are available.  The server can orchestrate
  connection use across the cluster as a whole, routing larger sets
  of connections to applications that need it most, while maintaining database-wide
  connectivity limits.  Even among hundreds of applications, each
  will gracefully block when target databases have no more connections
  available, rather than failing to connect.  This eliminates the need for
  "queueing" of connections at a proxy, and also uses less network
  overhead.

* **Eliminating excess idle connections** - The actual "pooling" of
  database connections locally within each application is an optional behavior.
  However, an issue with application-level connection pooling is that idle
  connections use up valuable database resources that may be needed elsewhere.
  HAAlchemy provides both the background mechanism to handle the task of
  idle connection maintenance for an otherwise idle application, as well as a
  useful estimate of how many idle connections should be released given the
  need elsewhere.

* **Load balancing, failover, and server routing** - Working with multi-master and
  master/failover architectures is achieved within the same
  scope as connection resource management.  The server performs health checks
  against all databases and applies the result through the configured routing
  algorithm.  Clients are given continuous instructions as to which hosts
  should receive connections and in what proportion.   For example,
  in a load-balanced scenario, clients have a set of connection slots including
  entries from all available hosts, and in a master/failover scenario,
  would have a set of connection slots that include only one host
  at a time.

* **Application-local connection pooling made resilient** - An array of issues
  related to long-lived connection pooling are solved.  Applications can be
  directed to different hosts and moved from failed or failover nodes
  proactively,    without the need   for applications to defensively reconnect on
  each use in order to be   on the correct host.  Stale connection checks such as
  ping-per-use   are no longer needed, and the issue of pooled connections stuck
  on a failover   node when the database has "failed back" is also solved.
  Connection pools   are made fully resilient against database restarts as well.

* **Centralized configuration** - Virtually everything about how an application
  connects to a database, including host, database name, authentication, connection
  limits, pooling limits, and timeouts, are delivered to it from the HAAlchemy
  daemon.  Applications themselves no longer require any database-specific
  configuration, only connectivity to the HAAlhcemy daemon itself.  Configuration
  and tuning parameters are live-updateable and take effect immediately
  across the cluster.  The HAAlchemy service itself is password-protected
  and can also use SSL certificates for authentication.

* **Rich monitoring and metrics** - HAAlchemy integrates the functionality
  of `connmon <https://bitbucket.org/zzzeek/connmon/src/954df26404d5533cf6a969b673e87b66ffe02a85/screenshot.png?fileviewer=file-view-default>`_, allowing full visibility
  into all database activity across the whole cluster under one interface,
  including not just per-host/process database connection use but also
  metrics on checkins, checkouts, and errors.

Other key features include:

* SQLAlchemy applications require no code changes.  The client is invoked via a
  SQLAlchemy plugin referred to within the string URL passed to
  ``create_engine()``.

* The HAAlchemy daemon can be stopped and restarted without affecting the
  clients.  Clients continue to run without it, making use of their most
  recent list of connection slots.  Upon auto-reconnection, they
  refresh the new daemon with their current state.

* No latency is added to the connection checkout operation, as the server only
  provides resource guidance asynchronously, and is not consulted
  on connection use.

* Multiple HAAlchemy daemon instances can be configured to form a
  cluster where state is shared among all nodes.  The Raft algorithm is used
  to elect a leader within clustered mode.

* Database health checks are performed using a persistent connection, no
  separate HTTP services or configurations are needed.

* Multiple routing algorithms are supported, and new ones can be dropped
  in using a few lines of Python code.   The available hosts and connection
  counts are given as input and the output is the host we should pick.

* By allowing client/server communication to remain direct, HAAlchemy does
  not have the massive throughput requirements of a proxy server.  By working
  asynchronously with respect to actual connection use, it can't become an
  application bottleneck, and as it maintains only a single communication
  channel with any given application process, has relatively low work
  requirements even in a cluster running maximum traffic across hundreds of
  processes and thousands of database connections.


Comparison to Other Approaches
==============================

Prominent tools within the problem space of database connection routing and pooling
are HAProxy, PGBouncer and SQLRelay.  These tools are all essentially proxy servers,
but all with very different behaviors and featuresets.   HAProxy and PGBouncer
have very little overlap in features at all, and SQLRelay has
more elaborate requirements.  It is therefore kind of interesting that
HAAlchemy is **not** a proxy
server, but seeks to accomodate the database-related use cases of all of these
tools at the same time.

HAProxy
-------

HAProxy is an HTTP and TCP proxy server intended for load balancing, and
is very widely used.  It is
commonly used in front of a Galera multi-master cluster to allow applications
to connect to any number of Galera nodes in a cluster, or to direct applications
at just a particular node at once, while allowing for failover to a different
node when a node is marked as down.  HAProxy's architecture in this scenario
is pictured in the following figure:

.. figure:: haproxy.png
    :alt: HAProxy Cluster

    HAProxy Cluster Architecture

HAProxy's use cases are tailored towards short-lived connections, notably for
massive numbers of HTTP connections where it excels. It of course also performs
extremely well with long-lived connections.   It also includes a feature
known as a "backup" node, which can be used to mark a load-balanced node
as being used only when the "primary" node is down, which is essential
for master/failover use, as well as is commonly applied to Galera multi-master
clusters.

HAProxy does not have ideal interactions with connections that are pooled by
the client, nor does it have any connection pooling capabilities itself.  As a
connection pool holds onto connections in an otherwise idle state, HAProxy
cannot transparently move an application's idle connections from one host to
another, as would be needed for smooth "failback" behavior.  In a
master/failover architecture, during failback to the master, pooled client
connections remain stuck on the failover node until they expire normally, leading to
database access being inappropriately split for this time period.  HAProxy
also cannot gracefully instruct the application to remove a pooled connection that's no
longer usable; outside of relying upon fixed timeouts in the application's
pool, for more sudden connection invalidation, the application can only be
forced to discover that a connection is stale by attempting to use the
connection and failing.

PGBouncer
---------

PGBouncer is, like HAProxy, a proxy server, however that's where its similarities
end.  PGBouncer's single purpose is to create a pool of Postgresql database
connections, which are available quickly to clients as they connect.  It proxies
the native Postgresql protocol, interacting between client and server to perform
the mechanics of linking up a multi-session server-side connection with
a single-session client connection, including handling logins, session
settings, and transaction scope.

Unlike HAProxy, PGBouncer explicitly has no load balancing or failover
functionality built in, deferring to other proxying tools that would accomplish
this, most notably HAProxy itself (where ironically we have the same pooled
connection issues at least if PGBouncer is placed in front of HAProxy).
Like HAProxy, PGBouncer is again not oriented towards application-level
connection pooling, as this would diminish PGBouncer's ability to make
efficient use of all connections.   Non-pooled applications therefore still
have the overhead of TCP connection and login on each connection use, just
less overhead than there would be making a Postgresql server connection
directly.

PGBouncer also only supports Postgresql.  Because connection pool middleware
must necessarily be involved with the specifics of the database protocol,
generalizing PGBouncer's architecture is a difficult problem.

SQLRelay
--------

SQLRelay is a database proxy which provides both a middleware-based connection
pool and multiple database support with load balancing.   Unlike PGBouncer, it
takes on the ambitious task of supporting most major database platforms. This
seems to have resulted in a `complicated licensing situation
<http://sqlrelay.sourceforge.net/license.html>`_, but more critically SQLRelay
appears to `implement its own wire protocol
<https://sourceforge.net/p/sqlrelay/mailman/message/27648295/>`_, rather than
simply proxying the wire protocol of the proxied database the way HAProxy and
PGBouncer do.  This is a deep tradeoff which means SQLRelay `requires its own
client be used
<http://sqlrelay.sourceforge.net/sqlrelay/programming/pythondb.html>`_, rather
than allowing the use of well known database clients and vendor-supported
client APIs.  Delivering its own wire protocol means the server must work not
only to proxy database traffic but to translate all of it between vendor-
specific and its own system.

SQLRelay's load balancing feature does not
appear to support the "backup" node concept, where a single master database is
always preferred and clients will "fail back" to this database if it returns
from an outage.  It also does not appear to provide any support for asynchronous
management of application-pooled connections, even though it requires use
of its own client libraries.

Comparison Matrix
-----------------

The following matrix illustrates a cross-comparison of HAAlchemy,
HAProxy, PGBouncer and SQLRelay features:

+---------------------------------------------+----------+-----------+--------------+--------------+
|Feature                                      |  HAProxy | PGBouncer |   SQLRelay   | HAAlchemy    |
+=============================================+==========+===========+==============+==============+
|**General**                                                                                       |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Supported databases                          | any TCP- | Postgresql| most major   | any with     |
|                                             | based    | only      | databases    | Python DBAPI |
|                                             |          |           |              | support      |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Clients speak DB-native wire protocol        |  yes     | yes       | no           | yes          |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Supported application languages              |  any     |    any    | most major   | Python [*]_  |
|                                             |          |           | languages    |              |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Supported database client APIs (Python)      |   any    |    any    | SQLRelay     | any pep-249  |
|                                             |          |           | PySQLRDB     | DBAPI [*]_   |
|                                             |          |           | only         |              |
+---------------------------------------------+----------+-----------+--------------+--------------+
|License                                      |   GPL    |    BSD    | Multiple     | MIT          |
+---------------------------------------------+----------+-----------+--------------+--------------+
|**Pooling**                                                                                       |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Cross-cluster connection pooling             |    no    |     yes   |     yes      | yes          |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Application level connection pooling support |    no    |     no    |     no       | yes          |
|(either as a feature, or accommodated)       |          |           |              |              |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Connection queueing (e.g. block until        |   yes    |     yes   |     yes      | yes          |
|available)                                   |          |           |              |              |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Socket-less queueing                         |    no    |     no    |     no       | yes          |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Removal of stale application-pooled          |app must  | app must  |app must      | transparent  |
|connections                                  |handle DB | handle DB |handle DB     |              |
|                                             |error on  | error on  |error on      |              |
|                                             |connection| connection|connection    |              |
|                                             |use       | use       |use           |              |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Asynchronous management of excess            |  no      |    no     | no           | transparent  |
|application-pooled idle connections          |          |           |              |              |
+---------------------------------------------+----------+-----------+--------------+--------------+
|**Multi-Host**                                                                                    |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Load balancing                               |  yes     |     no    |     yes      | yes          |
+---------------------------------------------+----------+-----------+--------------+--------------+
|DB health checks w/ failover                 |  yes     |     no    |     yes      | yes          |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Master/failover support (e.g. talk to only   |  yes     |     no    |     no       | yes          |
|one database at a time, failback all clients |          |           |              |              |
|to original master when it returns)          |          |           |              |              |
+---------------------------------------------+----------+-----------+--------------+--------------+
|seamless failback w/ application-pooled      |  no      |    N/A    |     N/A      | yes          |
|connections                                  |          |           |              |              |
+---------------------------------------------+----------+-----------+--------------+--------------+
|**HA**                                                                                            |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Applications can run when service is         |    no    |     no    |     no       | yes          |
|down / restarting                            |          |           |              |              |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Applications unaffected by service latency   |    no    |     no    |     no       | yes          |
+---------------------------------------------+----------+-----------+--------------+--------------+
|Clustering of the service itself             |    yes   |     no    |     yes      | yes          |
+---------------------------------------------+----------+-----------+--------------+--------------+


.. [*] Other programming languages and platforms can,
       in theory, also support clients for the HAAlchemy daemon.

.. [*] For SQLAlchemy applications, this would be a `SQLAlchemy supported
       DBAPI <http://docs.sqlalchemy.org/en/latest/dialects/index.html>`_ (which include all the most common ones), however SQLAlchemy's `DBAPI level pooling <http://docs.sqlalchemy.org/en/latest/core/pooling.html#pooling-plain-db-api-connections>`_ use case, which
       works for any Python DBAPI, is also supported.



