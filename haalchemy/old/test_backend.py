from haalchemy import config
from haalchemy import backend as _backend
from sqlalchemy.engine import url

import unittest


class _ConfigFixture(object):
    def _config_fixture(self):
        return config.Config.from_config_string("""

[haalchemy_cluster_rhel-cluster]
name: rhel-cluster
base_url: mysql+mysqldb://root:root@<hostname>/test
database_impl: galera
max_connection_age: 30

strategy: leastconn

hosts:
    host1 hostname=host1
    host2 hostname=host2

""")


class URLTest(_ConfigFixture, unittest.TestCase):

    def test_url_databasename(self):
        """test that the database name in the given URL is passed down
        to the config and into the host."""

        config = self._config_fixture()

        cluster_config = config.for_url(
            url.make_url("haalchemy://A:X@rhel-cluster/dbname"))
        backend = _backend.ClusterBackend.from_config(cluster_config)
        self.assertEqual(
            backend.hosts['host1'].url,
            url.make_url("mysql+mysqldb://root:root@host1/dbname")
        )

        # make sure it stays separate on another run
        cluster_config = config.for_url(
            url.make_url("haalchemy://A:X@rhel-cluster/otherdbname"))
        backend = _backend.ClusterBackend.from_config(cluster_config)
        self.assertEqual(
            backend.hosts['host1'].url,
            url.make_url("mysql+mysqldb://root:root@host1/otherdbname")
        )
