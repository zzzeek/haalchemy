import copy
import os
import time
import warnings

from sqlalchemy import exc as sqla_exc
from sqlalchemy import event
import weakref
import logging

from . import backend as _backend
from . import strategy as _strategy
from . import exc as _exc
from . import status
from . import util
from .state import StateMachine

async_suite = util.async_suite(green=False)

_frontends = []


log = logging.getLogger(__name__)


class ClusterFrontend(util.Startable):
    def __init__(self, engine, cluster_config):
        self.engine = engine
        self.cluster_config = cluster_config

        if not cluster_config.get('base_url'):
            base_url = copy.copy(engine.url)
            base_url.hostname = "<hostname>"
            cluster_config['base_url'] = base_url

        strategy_name = cluster_config['strategy']
        try:
            self.strategy_fn = _strategy._strategy_cls[strategy_name]
        except KeyError:
            raise ValueError("unknown strategy: %r" % strategy_name)

        self.poll_interval = cluster_config['poll_interval']
        self.idle_timeout = cluster_config['idle_timeout']
        self.max_connection_age = cluster_config['max_connection_age']
        self.reap_interval = cluster_config['reap_interval']
        self.monitor_servicename = cluster_config['monitor_servicename']

        self.cluster_backend = \
            _backend.ClusterBackend.from_config(cluster_config)
        self.name = self.cluster_backend.name
        self.hosts = dict(
            (host.name, HostConnections(self, host))
            for host in self.cluster_backend.hosts.values()
        )
        util.setup_listeners(self)

        self._setup_events()
        self.pid = os.getpid()

        if engine.url.query.get('start', False) == 'true':
            self.start()

    def do_start(self):
        if self not in _frontends:
            _frontends.append(self)

        self._start_new_monitors()

    def do_stop(self):
        if self in _frontends:
            _frontends.remove(self)
        self.status_client.stop()
        self.cluster_backend.close_connections()

    def _setup_child_fork_monitoring(self):
        self._start_new_monitors()

    def _start_new_monitors(self):
        if self.monitor_servicename:
            app_config = self.cluster_config['app_config']

            self.status_client = status.RemoteStatusUpdater(
                app_config, self.cluster_backend,
                self.monitor_servicename)
        else:
            self.status_client = status.LocalStatusUpdater(
                self.cluster_config, self.cluster_backend)
        self.strategy = self.strategy_fn(
            self.cluster_config, self.cluster_backend)

        self.status_client.start()

    @util.listens_for("cluster_backend", "host_up")
    def _host_up(self, cluster_backend, host, rec, is_local):
        self.hosts[host.name]._host_up()

    @util.listens_for("cluster_backend", "host_down")
    def _host_down(self, cluster_backend, host, rec, is_local):
        self.hosts[host.name]._host_down()

    def host_is_available(self, name):
        return self.cluster_backend.host_is_available(name)

    @classmethod
    def from_config(cls, engine, cfg):
        return ClusterFrontend(engine, cfg)

    def _setup_events(self):
        event.listen(self.engine, "do_connect", self._sqla_event_connect)
        event.listen(self.engine, "checkout", self._sqla_event_checkout)
        event.listen(self.engine, "checkin", self._sqla_event_checkin)
        event.listen(
            self.engine, "invalidate", self._sqla_event_hard_invalidate)
        event.listen(
            self.engine, "soft_invalidate", self._sqla_event_soft_invalidate)

        event.listen(
            self.engine, "handle_error", self._sqla_event_handle_error)

        event.listen(
            self.engine, "engine_disposed", self._sqla_event_engine_disposed)

    def _sqla_event_connect(
            self, dialect, connection_record, cargs, cparams):
        self.start()

        _check_threads_started(self)

        return _ConnectionRec.connect(self, connection_record)

    def _sqla_event_checkout(
            self, dbapi_connection, connection_record, wrapper):

        _ConnectionRec.for_connection_rec(
            connection_record)._sqla_event_checkout(
                dbapi_connection, connection_record)

    def _sqla_event_checkin(self, dbapi_connection, connection_record):
        _ConnectionRec.for_connection_rec(
            connection_record)._sqla_event_checkin(
                dbapi_connection, connection_record)

    def _sqla_event_hard_invalidate(
            self, dbapi_connection, connection_record, exception):
        _ConnectionRec.for_connection_rec(
            connection_record)._sqla_event_hard_invalidate(
            dbapi_connection, connection_record, exception)

    def _sqla_event_soft_invalidate(
            self, dbapi_connection, connection_record, exception):
        _ConnectionRec.for_connection_rec(
            connection_record)._sqla_event_soft_invalidate(
            dbapi_connection, connection_record, exception)

    def _sqla_event_handle_error(self, ctx):
        ctx.invalidate_pool_on_disconnect = False
        connection = ctx.connection
        if connection is None:
            return
        conn_wrapper = connection.connection
        if conn_wrapper is None:
            return
        connection_record = conn_wrapper._connection_record
        if connection_record is not None:
            _ConnectionRec.for_connection_rec(
                connection_record)._sqla_event_handle_error(ctx)

    def _sqla_event_engine_disposed(self, engine):
        self.stop()


class HostConnections(object):
    """Represent a set of connections associated with a host."""

    def __init__(self, frontend, host):
        self.frontend = frontend
        self.name = host.name
        self.host = host
        self.pid = os.getpid()
        self._connection_records = set()
        self._last_reaped_time = frontend.cluster_backend._counter()
        self._periodic_reaper_sweep = util.periodic_timer(
            frontend.reap_interval)
        self._connections = frontend.cluster_backend._counter()
        self._checkouts = frontend.cluster_backend._counter()
        self._subprocess_status = _backend.Host.START

    def _host_up(self):
        pass

    def _host_down(self):
        # because we are coming from a worker thread, this
        # essentially just sets a "we are invalid as of
        # time.time()" attribute on the connection record, and
        # that's it, pretty much the same way the DB itself can go
        # down at any moment as well.  Everything else happens
        # within the same thread that the rec is being used within
        # as a result of the "it's down!" observation.
        for crec in list(self._connection_records):
            crec.invalidate_connection_rec()

    def status_string(self):
        return (
            "frontend: %s Host: %s %s "
            "Total database server connections: %s "
            "Current local connections: %d "
            "Current local checkouts: %d Disconnect error count: %d" % (
                self.frontend.name,
                self.name,
                self.host.state_str(),
                self.host.server_connections,
                self._connections.value,
                self._checkouts.value,
                self.host.error_count,
            )
        )

    def _handle_sqla_disconnect(self, ctx):
        log.info("Disconnect detected on host %s", self.name)
        self.host.inc_error_count()

    def _reaper_sweep(self, now):
        if not self.host.up:
            return

        elif not self.frontend.idle_timeout and \
                not self.frontend.max_connection_age:
            return

        if not self._periodic_reaper_sweep(now):
            return

        for rec in self._connection_records:
            if rec.check_reap(now, self._last_reaped_time):
                # if a reap occurs, the next reap is on the next
                # pass
                break


class _ConnectionRec(object):
    __slots__ = (
        'frontend', 'connection_record', 'pid',
        'host_name', 'state', '__weakref__',
        'statemachine', '_checkout_mutex', 'last_checkin',
        'last_connected'
    )

    state = StateMachine()
    START = state.START
    CHECKEDIN = state.new("CHECKEDIN")
    CHECKEDOUT = state.new("CHECKEDOUT")
    INVALIDATED_CHECKEDIN = state.new("INVALIDATED_CHECKEDIN")
    INVALIDATED_CHECKEDOUT = state.new("INVALIDATED_CHECKEDOUT")
    NOHOST = state.new("NOHOST")
    FINALIZED = state.new("FINALIZED")

    state.null(START, NOHOST)
    state.null(NOHOST, FINALIZED)

    # these occur when the pool attempts the initial
    # connection before the "checkout" event, and the connection
    # attempt fails
    state.bounce(INVALIDATED_CHECKEDIN, CHECKEDIN)
    state.bounce(CHECKEDIN, CHECKEDIN)

    # can occur if worker thread sends us a soft invalidate
    # when we were already there
    state.null(INVALIDATED_CHECKEDIN, INVALIDATED_CHECKEDIN)

    @state.transition(CHECKEDIN, CHECKEDOUT, interesting=False)
    @state.transition(INVALIDATED_CHECKEDIN, CHECKEDOUT)
    def _checkin_to_checkout(self, from_, to, connection_record):
        host_connections = self.frontend.hosts[self.host_name]

        if connection_record.connection is None:
            raise sqla_exc.DisconnectionError(
                "Connection record was invalidated"
            )
        elif not host_connections.host.up:
            # this sends the record through the invalidate + reconnect
            # workflow, where we'll come back here again after a
            # new dialect.do_connect() call.
            raise sqla_exc.DisconnectionError(
                "host %s is no longer available" % self.host_name)
        elif self.pid != os.getpid():
            log.info(
                "%s detected pid change from %s to %s, reconnecting",
                self, self.pid, os.getpid())

            try:
                raise sqla_exc.DisconnectionError(
                    "Pid %s does not match %s, need to reconnect" %
                    (os.getpid(), self.pid)
                )
            finally:
                # we've been copied from parent process to child process.
                # we are already in the parent._connection_records list,
                # but now we count as a new connection record
                self.pid = os.getpid()
                if from_ is self.CHECKEDIN:
                    self._update_connections(host_connections, 1)
        else:
            if from_ is self.INVALIDATED_CHECKEDIN:
                log.info("host %s INVALIDATED_CHECKEDIN -> checkedout" % self)
                self._update_connections(host_connections, 1)

            self._update_checkouts(host_connections, 1)

    @state.transition(CHECKEDOUT, CHECKEDIN, interesting=False)
    @state.transition(INVALIDATED_CHECKEDOUT, INVALIDATED_CHECKEDIN)
    def _checkout_to_checkedin(self, from_, to):
        host_connections = self.frontend.hosts[self.host_name]
        self._update_checkouts(host_connections, -1)
        now = time.time()
        self.last_checkin = now

    @state.transition(NOHOST, CHECKEDIN)
    def _nohost_to_checkedin(self, from_, to, host_connections):
        log.info("%s moving to host %s", self, host_connections.name)
        self.host_name = host_connections.name

        assert self not in host_connections._connection_records
        host_connections._connection_records.add(self)
        self._update_connections(host_connections, 1)

        self.last_checkin = time.time()

    @state.transition(CHECKEDIN, INVALIDATED_CHECKEDIN)
    @state.transition(CHECKEDOUT, INVALIDATED_CHECKEDOUT)
    def _to_invalidated(self, from_, to):
        host_connections = self.frontend.hosts[self.host_name]
        self._update_connections(host_connections, -1)

    @state.transition(CHECKEDIN, NOHOST)
    @state.transition(INVALIDATED_CHECKEDIN, NOHOST)
    @state.transition(INVALIDATED_CHECKEDOUT, NOHOST)
    def _to_nohost(self, from_, to):
        log.info("%s leaving host", self)
        host_name = self.host_name
        assert self.host_name is not None
        self.host_name = None
        host_connections = self.frontend.hosts[host_name]

        host_connections._connection_records.remove(self)
        if from_ is self.CHECKEDIN:
            self._update_connections(host_connections, -1)

        if from_ is self.INVALIDATED_CHECKEDOUT:
            self._update_checkouts(host_connections, -1)

    # 1. pool overflow makes temporary connection records during overflow
    # 2. a new child proc that calls engine.dispose() will carry over
    # conn recs from the parent before this is called
    @state.transition(CHECKEDIN, FINALIZED)
    @state.transition(INVALIDATED_CHECKEDIN, FINALIZED)
    def _checkin_to_finalized(self, from_, to):
        host_name = self.host_name
        assert host_name is not None  # else we should be NOHOST

        host_connections = self.frontend.hosts[host_name]

        host_connections._connection_records.remove(self)
        if from_ is self.CHECKEDIN:
            self._update_connections(host_connections, -1)

    def _update_connections(self, host_connections, delta):
        # don't count for recs that leaked in from the parent process!

        assert self.pid == os.getpid()

        with host_connections._connections.lock():
            host_connections._connections.delta(delta)

        # TODO: figure out a nicer API for this
        if host_connections.host._server_connections > -1:
            host_connections.host._server_connections.delta(delta)

    def _update_checkouts(self, host_connections, delta):
        # don't count for recs that leaked in from the parent process!

        assert self.pid == os.getpid()

        with host_connections._checkouts.lock():
            host_connections._checkouts.delta(delta)
            # print(
            #    "host %s state %s pid %s / %s checkouts %s value %s" % (
            #        self.host_name, self.state.str(self), os.getpid(),
            #       self.pid, delta, host_connections._checkouts.value)
            # )

    def transition_host(self, host_connections):
        if host_connections.name == self.host_name:
            return

        if not self.NOHOST.current(self):
            self.NOHOST.go(self)
        self.CHECKEDIN.go(self, host_connections)

    def invalidate_connection_rec(self, soft=True):
        connection_record = self.connection_record()
        if connection_record is not None:
            connection_record.invalidate(soft=soft)

    def __str__(self):
        return "ha-connection(host=%s, state=%s, id=0x%x)" % (
            self.host_name, self.state.str(self), id(self)
        )

    def __init__(self, frontend, connection_record):
        # direct patch instead of .info(), so that we
        # stay attached through reconnects etc.
        connection_record._haalchemy_rec = self
        self.state.init_instance(self)
        self.frontend = frontend
        self.connection_record = weakref.ref(connection_record, self._cleanup)
        self.pid = os.getpid()
        self._checkout_mutex = async_suite.lock()
        self.host_name = None
        self.last_checkin = None
        self.last_connected = None
        self.NOHOST.go(self)

    def _cleanup(self, ref):
        self.FINALIZED.go(self)

    @classmethod
    def connect(cls, frontend, connection_rec):
        if hasattr(connection_rec, '_haalchemy_rec'):
            host_record = connection_rec._haalchemy_rec
        else:
            host_record = _ConnectionRec(frontend, connection_rec)

        log.info("%s connect", host_record)
        try:
            host = frontend.strategy.get_host()
        except _exc.NoHostsAvailableException:
            raise _exc.CantConnectException("No hosts available")
        else:
            host_connections = frontend.hosts[host.name]
            host_record.transition_host(host_connections)
            try:
                conn = host.connect()
            except:
                raise
            else:
                host_record.pid = os.getpid()
                host_record.last_connected = time.time()
                return conn

    @classmethod
    def for_connection_rec(cls, rec):
        return rec._haalchemy_rec

    def check_reap(self, now, last_reaped_time):
        # not in checkedin state
        if not self.CHECKEDIN.current(self):
            return False

        timeout = self.frontend.idle_timeout
        max_age = self.frontend.max_connection_age

        isage = max_age and self.last_connected and \
            (now - self.last_connected > max_age)
        istimeout = timeout and self.last_checkin and \
            (now - self.last_checkin > timeout)
        if not isage and not istimeout:
            return False

        # if another connection was reaped already, including
        # in another child process, don't reap until next pass.
        with last_reaped_time.lock():
            if now - last_reaped_time.value < self.frontend.reap_interval:
                return
            else:
                last_reaped_time.set(int(now))

        # OK maybe we will reap...
        if not self._checkout_mutex.acquire(False):
            # apparently we are being checked out at the moment
            # we thought we'd invalidate an idle connection.
            return False
        try:
            log.info(
                "%s invalidate %s connection, idle time %s connection age %s",
                self, "idle" if istimeout else "old",
                now - self.last_checkin, now - self.last_connected)
            self.invalidate_connection_rec(soft=False)
            return True
        finally:
            self._checkout_mutex.release()

    def _sqla_event_checkout(self, connection, connection_record):
        with self._checkout_mutex:
            self.CHECKEDOUT.go(self, connection_record)

    def _sqla_event_checkin(self, connection, connection_record):
        if self.INVALIDATED_CHECKEDOUT.current(self):
            self.INVALIDATED_CHECKEDIN.go(self)
        else:
            self.CHECKEDIN.go(self)

    def _sqla_event_hard_invalidate(
            self, connection, connection_record, exception):
        if self.CHECKEDOUT.current(self):
            self.INVALIDATED_CHECKEDOUT.go(self)
        elif self.CHECKEDIN.current(self):
            self.INVALIDATED_CHECKEDIN.go(self)

    def _sqla_event_soft_invalidate(
            self, connection, connection_record, exception):
        if self.CHECKEDOUT.current(self):
            self.INVALIDATED_CHECKEDOUT.go(self)
        else:
            self.INVALIDATED_CHECKEDIN.go(self)

    def _sqla_event_handle_error(self, ctx):
        if ctx.is_disconnect:
            host_connections = self.frontend.hosts[self.host_name]
            host_connections._handle_sqla_disconnect(ctx)


_worker_thread = None

_pid = os.getpid()


def _check_threads_started(frontend):
    global _worker_thread, _pid
    ospid = os.getpid()

    if _worker_thread is None or _pid != ospid:

        if _pid != ospid:
            frontend._setup_child_fork_monitoring()

        _pid = ospid
        log.info("Starting worker thread for pid %s", _pid)

        _worker_thread = async_suite.background_thread(_process_worker)


def _process_worker():
    while True:
        now = time.time()
        for frontend in _frontends:
            for host_connections in frontend.hosts.values():
                host_connections._reaper_sweep(now)

        time.sleep(.1)


