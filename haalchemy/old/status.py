from . import exc
from .. import status as _status
from .. import compat
from .. import util
import logging
from .. import database
from .. import backend as _backend
from . import rpc
from . import raft
from . import server as _server
from . import client as _client


log = logging.getLogger(__name__)


class StatusMonitorService(_server.Plugin):
    """Uses a server.RemoteServer as transport to serve DB status commands."""

    name = 'status_monitor'

    def __init__(self, service):
        self.service = service
        self.monitors = {}
        self.async_suite = service.async_suite

        for clustername in self.service.service_config['monitor_cluster']:
            cluster_config = service.config.config_for_clustername(clustername)
            MonitoredCluster(self, cluster_config)

        service.add_plugin(self)

        nodes = self.service.service_config['nodes']
        self.is_clustered = len(nodes) > 1 and \
            self.service.service_config['cluster_servers']

        util.setup_listeners(self)


    def _start_db_ping(self):
        log.info(
            "Monitor server service %s node [%s] starting database listening",
            self.service.servicename,
            self.service.nodename
        )

        self._is_monitoring_directly = True
        for monitor in self.monitors.values():
            monitor.start()

    def _stop_db_ping(self):

        log.info(
            "Monitor server service %s node [%s] stopping "
            "direct database listening",
            self.service.servicename,
            self.service.nodename
        )

        self._is_monitoring_directly = False
        for monitor in self.monitors.values():
            monitor.stop()

    def before_listen(self):
        # threaded queue is intentional - as this queue receives
        # DB status messages that are polled within a real thread,
        # as DBAPIs don't support asynchronous calling.   The queue
        # is populated from a real thread and consumed from a green thread.
        self._threaded_queue = compat.queue.Queue(100)
        self._deliver_messages_greenlet = self.async_suite.background_thread(
            self._deliver_db_status_messages
        )

    def _queue_status_message(self, monitor, host, rec, is_local):
        try:
            self._threaded_queue.put_nowait((monitor, host))
        except compat.queue.Full:
            log.warn(
                "Server message queue is full! "
                "dropped message from host: %s", host.name)
        else:
            log.debug(
                "Received %s DB status event %s",
                "local" if is_local else "remote peer", rec)

    def _deliver_db_status_messages(self):
        """Broadcast DatabaseStatus events to listening clients and peers."""

        while True:
            try:
                monitored_cluster, host = self._threaded_queue.get_nowait()
            except compat.queue.Empty:
                self.async_suite.sleep(.1)
            else:
                msg = self._host_status_message(monitored_cluster, host)
                # send to listening clients
                for server_connection in self.service.connections:
                    if monitored_cluster.name in \
                            self._client_listening(server_connection):
                        msg.send_event(server_connection.rpc_service)

                # if we are RAFT leader, send to peers
                if self.is_clustered and self.raft.is_leader:
                    for peer in self.p2p.peers:
                        if peer.connected:
                            msg.send_event(peer.rpc_service)

    def _monitored_for_clustername_arg(self, clusternames):
        if clusternames:
            try:
                return[
                    self.monitors[clustername]
                    for clustername in clusternames
                ]
            except KeyError as ke:
                raise exc.CommandError("No such cluster: %s" % ke.args[0])
        else:
            return self.monitors.values()

    def _cmd_listen(self, server_connection, clusternames):
        monitored = self._monitored_for_clustername_arg(clusternames)
        self._client_listening(server_connection).update(
            [m.name for m in monitored])

        for m in monitored:
            for h in m.cluster_backend.hosts.values():
                self._host_status_message(m, h).\
                    send_event(server_connection.rpc_service)

    def _host_status_message(self, monitored_cluster, host):
        return DatabaseStatus(
            self._host_status_line(monitored_cluster, host),
            self.service.nodename)

    def _cmd_unlisten(self, server_connection, clusternames):
        monitored = self._monitored_for_clustername_arg(clusternames)
        self._client_listening(server_connection).difference_update(
            [m.name for m in monitored])

    def _cmd_info(self, clustername=None, hostname=None):
        monitored = self._monitored_for_clustername_arg(
            [clustername] if clustername else [])

        retval = []
        for monitor in monitored:
            if hostname:
                try:
                    hosts = [monitor.cluster_backend.hosts[hostname]]
                except KeyError:
                    raise exc.CommandError(
                        "No such host in %s: %r" %
                        (monitor.name, hostname))
            else:
                hosts = list(monitor.cluster_backend.precedence_ordered_hosts)
            for host in hosts:
                retval.append(self._host_status_line(monitor, host, info=True))
        return retval

    def _cmd_status(self, clusternames):
        monitored = self._monitored_for_clustername_arg(clusternames)

        return [
            self._host_status_line(monitored_cluster, host)
            for monitored_cluster in monitored
            for host in monitored_cluster.cluster_backend.
            precedence_ordered_hosts
        ]

    def _cmd_getconfig(self, clusternames):
        monitored = self._monitored_for_clustername_arg(clusternames)

        return [
            monitored_cluster.cluster_config
            for monitored_cluster in monitored
        ]

    def _host_status_line(self, monitored_cluster, host, info=False):
        rec = {
            "cluster_name": monitored_cluster.name,
            "host_name": host.name,
            "status": "UP" if host.up else "DOWN",
            "position": host.position,
            "connections": host.server_connections,
            "up_since": host.up_since_isoformat
        }
        if info:
            rec.update({
                "address": host.hostname,
                "port": host.port
            })
        return rec


class MonitoredCluster(util.Startable):
    """Monitors a cluster.

    This object mediates the job of pinging databases with that of delivering
    events to the StatusMonitorService which broadcasts this status to clients
    and peers.

    In the case that our StatusMonitorService is a peer in "follower" mode,
    the StatusMonitorService also sends database status events from the
    "leader" peer to this object, where they get queued for broadcast
    to local clients only.

    """

    def __init__(self, status_monitor, cluster_config):
        self.name = cluster_config['name']
        self.cluster_config = cluster_config
        self.cluster_backend = _backend.ClusterBackend.from_config(
            cluster_config)
        self.status_monitor = status_monitor
        self.service = status_monitor.service
        self.status_monitor.monitors[self.name] = self
        util.setup_listeners(self)

        # set up database status pingers which will send
        # events to the ClusterBackend.
        # when using raft, we start and stop this
        # based on if we are the leader or not.  when not the leader,
        # we propagate events from peers to the cluster_backend
        # instead.
        self.status_client = _status.LocalStatusUpdater(
            self.cluster_config, self.cluster_backend)

    def do_start(self):
        self.status_client.start()

    def do_stop(self):
        self.status_client.stop()
        self.cluster_backend.close_connections()

    def receive_p2p_status_event(self, rpc_evt):
        message, host = rpc_evt.as_database_status(
            self.cluster_backend)
        host.set_status(message)

    @util.listens_for("cluster_backend", "periodic_broadcast")
    def _host_status_update_threadsafe(
            self, cluster_backend, host, rec, is_local):
        self.status_monitor._queue_status_message(self, host, rec, is_local)

    @util.listens_for("cluster_backend", "host_up")
    def _host_up(
            self, cluster_backend, host, rec, is_local):
        self.status_monitor._queue_status_message(self, host, rec, is_local)

    @util.listens_for("cluster_backend", "host_down")
    def _host_down(
            self, cluster_backend, host, rec, is_local):
        self.status_monitor._queue_status_message(self, host, rec, is_local)


class StatusMonitorClient(_client.Plugin):
    """Uses a client.LocalClient as transport to run DB status commands."""

    name = 'status_monitor'

    def __init__(self, client):
        self.client = client
        self.client.add_plugin(self)
        raft.RaftClientCommands(self.client)
        self.rpc_service = client.rpc_service
        self.rpc_service.listen(
            self._service_database_status_event, DatabaseStatus)

    def connect(self):
        self.client.connect()

    def _service_database_status_event(self, evt):
        """Receive a DatabaseStatus RPC event from the server we're connected
        to, propagate to listeners.

        """
        self.dispatch.database_status(self, evt)

    def cmd_info(self, clustername=None, hostname=None):
        return Info(clustername, hostname).send(self.rpc_service)

    def cmd_status(self, *clusterconfigs):
        return Status(clusterconfigs).send(self.rpc_service)

    def cmd_listen(self, *clusterconfigs):
        return Listen(clusterconfigs).send(self.rpc_service)

    def cmd_unlisten(self, *clusterconfigs):
        return Unlisten(clusterconfigs).send(self.rpc_service)

    def cmd_getconfig(self, *clusterconfigs):
        return GetConfig(clusterconfigs).send(self.rpc_service)


class StatusMonitorListener(util.EventListener):
    _dispatch_target = StatusMonitorClient

    def database_status(self, client, evt):
        pass


@rpc.RPC.base('clusternames')
class Listen(rpc.RPC):
    def receive_request(self, rpc, service_connection):
        status_monitor = service_connection.server.plugins['status_monitor']
        return status_monitor._cmd_listen(
            service_connection, self.clusternames)


@rpc.RPC.base('clusternames')
class Unlisten(rpc.RPC):
    def receive_request(self, rpc, service_connection):
        status_monitor = service_connection.server.plugins['status_monitor']
        return status_monitor._cmd_unlisten(
            service_connection, self.clusternames)


@rpc.RPC.base('clustername', 'hostname')
class Info(rpc.RPC):
    def receive_request(self, rpc, service_connection):
        status_monitor = service_connection.server.plugins['status_monitor']
        return status_monitor._cmd_info(self.clustername, self.hostname)


@rpc.RPC.base('clusternames')
class GetConfig(rpc.RPC):
    def receive_request(self, rpc, service_connection):
        status_monitor = service_connection.server.plugins['status_monitor']
        return status_monitor._cmd_getconfig(self.clusternames)


@rpc.RPC.base('clusternames')
class Status(rpc.RPC):
    def receive_request(self, rpc, service_connection):
        status_monitor = service_connection.server.plugins['status_monitor']
        return status_monitor._cmd_status(self.clusternames)


@rpc.RPC.base('status', 'origin')
class DatabaseStatus(rpc.RPC):
    def receive_request(self, rpc, service):
        return None

    def as_database_status(self, cluster_backend):
        hostname = self.status['host_name']
        host = cluster_backend.hosts[hostname]
        return database.Status.result(
            host,
            self.status['status'] == 'UP',
            self.status['connections'],
            origin=self.origin
        ), host
