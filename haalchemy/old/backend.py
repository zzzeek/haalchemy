"""Represent a collection of database hosts and their current status,
including up/down as well as connection count.

"""
import copy
import datetime
import time

import logging

from . import database as _database
from . import util
from . import exc as _exc
from .state import StateMachine

log = logging.getLogger(__name__)


class ClusterBackend(object):
    def __init__(self, name, base_url, cluster_config):
        self.name = name
        self.base_url = base_url

        # TODO: see if we can use engine for this, or
        # something
        dialect_cls = self.base_url.get_dialect()
        dbapi = dialect_cls.dbapi()
        self.dialect = dialect_cls(dbapi=dbapi)

        database_impl = cluster_config['database_impl']
        try:
            self.database_impl = _database._impls[database_impl]
        except KeyError:
            # TODO: figure this out at config time?
            raise ValueError("unknown database impl %r" % database_impl)
        else:
            self.database_impl.validate(base_url)

        self.poll_interval = cluster_config['poll_interval']
        self.periodic_update_interval = \
            cluster_config['periodic_update_interval']
        self._available = []
        self.hosts = {}

    def close_connections(self):
        for host in self.hosts.values():
            host.disconnect()

    def _counter(self, start=0):
        return util.Num.instance(start=start)

    @property
    def precedence_ordered_hosts(self):
        unavailable = set(self.hosts).difference(self._available)

        return [
            self.hosts[key] for key in self._available
        ] + [
            self.hosts[key] for key in unavailable
        ]

    def get_available(self):
        return list(self._available)

    def host_is_available(self, name):
        return name in self._available

    @classmethod
    def from_config(cls, cfg):
        base_url = cfg['base_url']

        database = cfg.get('databasename')
        # see if outside URL specified an alternate database name
        if database is not None:
            base_url = copy.copy(base_url)
            base_url.database = database

        client = cls(
            name=cfg['name'],
            base_url=base_url,
            cluster_config=cfg
        )
        if cfg.get('hosts'):
            for host in cfg['hosts']:
                hostname = host['hostname']
                port = host['port']
                name = host.get('name', hostname)
                Host(client, name, hostname, port)
        else:
            hostname = client.base_url.host
            port = client.base_url.port
            Host(client, hostname, hostname, port)

        return client


class ClusterListener(util.EventListener):
    _dispatch_target = ClusterBackend

    def periodic_broadcast(self, cluster, host, rec, is_local):
        pass

    def host_up(self, cluster, host, rec, is_local):
        pass

    def host_down(self, cluster, host, rec, is_local):
        pass


class Host(object):
    state = StateMachine()
    START = state.START
    UP = state.new("UP")
    DOWN = state.new("DOWN")
    #OFFLINE = state.new("OFFLINE")

    state.null(DOWN, DOWN)
    state.null(UP, UP)

    #@state.transition(UP, OFFLINE)
    #@state.transition(DOWN, OFFLINE)
    #@state.transition(START, OFFLINE)
    #def _set_offline(self, from_, to, status=None):
    #    if status is not None:
    #        log.info("Cluster %s Host %s is OFFLINE",
    #                 self.cluster_backend.name, self.name)
    #        if from_ is self.UP:
    #            self.cluster_backend._available.remove(self.name)

    #@state.transition(OFFLINE, DOWN)
    @state.transition(UP, DOWN)
    @state.transition(START, DOWN)
    def _set_down(self, from_, to, status=None):
        if status is not None:
            log.info("Cluster %s Host %s is DOWN",
                     self.cluster_backend.name, self.name)
            if from_ is self.UP:
                self.cluster_backend._available.remove(self.name)
        self.up = False

    #@state.transition(OFFLINE, UP)
    @state.transition(DOWN, UP)
    @state.transition(START, UP)
    def _set_up(self, from_, to, status=None):
        if status is not None:
            log.info("Cluster %s Host %s is UP",
                     self.cluster_backend.name, self.name)
            self.cluster_backend._available.append(self.name)
            self.up_since = time.time()
            self._error_count.set(0)
        self.up = True

    def __init__(self, cluster_backend, name, hostname, port):
        self.cluster_backend = cluster_backend
        self.database_impl = cluster_backend.database_impl
        self.dialect = cluster_backend.dialect
        self.name = name
        self.hostname = hostname
        self.port = port
        self.url = copy.copy(self.cluster_backend.base_url)
        self.url.host = self.hostname

        self.url.port = self.port
        self.cargs, self.cparams = self.cluster_backend.\
            dialect.create_connect_args(self.url)
        self.cluster_backend.hosts[self.name] = self
        self._check_connection = None
        self._server_connections = cluster_backend._counter(-1)
        self._error_count = cluster_backend._counter()
        self._host_check = \
            util.periodic_timer(self.cluster_backend.poll_interval)
        self._periodic_status_broadcast = util.periodic_timer(
            self.cluster_backend.periodic_update_interval)
        self.up_since = None
        self.up = False
        self.state.init_instance(self)

    def inc_error_count(self):
        """increment the error count, indicating a disconnect situation
        has occurred on this host.

        """
        self._error_count.delta(1)

    def _needs_check(self, now):
        if self.UP.current(self) and self._error_count.value > 0:
            return True
        else:
            return self._host_check(now)

    def disconnect(self):
        """Disconnect local DB connections when we are not performing
        checks ourselves."""

        if self._check_connection is None:
            return
        try:
            self._check_connection.close()
        except:
            log.info("Error occurred closing ping connection", exc_info=True)
        finally:
            self._check_connection = None

    def check_status(self, now, force=False):
        """set up/down status by hitting the database directly."""
        if not force and not self._needs_check(now):
            return

        if not self._check_connection:
            try:
                self._check_connection = self.connect(raise_plain=False)
            except _exc.ConnectionFailed:
                self._check_connection = None
                self._set_status(
                    _database.Status.result(self, False), now, True)
                return

        try:
            result = self.database_impl.check(self, self._check_connection)
        except _exc.OperationFailed:
            if self._check_connection:
                try:
                    self.database_impl.close(self, self._check_connection)
                except _exc.OperationFailed:
                    pass
                self._check_connection = None
            self._set_status(_database.Status.result(self, False), now, True)
        else:
            self._set_status(result, now, True)

    def _set_status(self, rec, now, local):
        """set up/down status from an internal check"""
        self._server_connections.set(rec.connection_count)
        if rec.up and not self.UP.current(self):
            self.UP.go(self, rec)
            self.cluster_backend.dispatch.host_up(
                self.cluster_backend, self, rec, local
            )
        elif not rec.up and not self.DOWN.current(self):
            self.DOWN.go(self, rec)
            self.cluster_backend.dispatch.host_down(
                self.cluster_backend, self, rec, local
            )
        elif self._periodic_status_broadcast(now):
            self.cluster_backend.dispatch.periodic_broadcast(
                self.cluster_backend, self, rec, local
            )

    def set_status(self, rec):
        """set up/down status from an external check"""

        self._set_status(rec, time.time(), False)

    def state_str(self):
        return self.state.str(self)

    @property
    def position(self):
        """our position in the 'available' list."""

        if self.up:
            try:
                return self.cluster_backend._available.index(self.name)
            except ValueError:
                return -1
        else:
            return -1

    @property
    def up_since_isoformat(self):
        if not self.up_since:
            return "N/A"
        else:
            return datetime.datetime.fromtimestamp(self.up_since).isoformat()

    @property
    def server_connections(self):
        """return the current estimated count of connections on this host
        across all applications, e.g. server-side host count.

        """
        return self._server_connections.value

    @property
    def error_count(self):
        """Return the current count of errors logged via the
        :meth:`.inc_error_count` method.

        """
        return self._error_count.value

    def connect(self, raise_plain=True):
        return self.database_impl.connect(self, raise_plain=raise_plain)
