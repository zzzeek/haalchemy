from haalchemy import config
from haalchemy import frontend as _frontend
from haalchemy import backend as _backend

from sqlalchemy import util as sqla_util
import unittest
import mock


class _ConfigFixture(object):
    def _config_fixture(self):
        return config.Config.from_config_string("""

[haalchemy_cluster_rhel-cluster]
name: rhel-cluster
base_url: mysql+mysqldb://root:root@<hostname>/test
database_impl: galera
max_connection_age: 30

strategy: leastconn

hosts:
    host1 hostname=host1
    host2 hostname=host2

""")


class ConnectionTest(_ConfigFixture, unittest.TestCase):

    def _fixture(self):
        config = self._config_fixture()
        cluster_config = config.clusters['rhel-cluster']
        cluster_backend = _backend.ClusterBackend.from_config(cluster_config)
        raw_host1 = cluster_backend.hosts['host1']
        raw_host2 = cluster_backend.hosts['host2']

        raw_host1.UP.go(raw_host1)
        raw_host2.UP.go(raw_host2)

        hosts = {}
        mock_frontend = mock.Mock(
            cluster_backend=cluster_backend,
            hosts=hosts
        )
        self.connection_record = connection_record = mock.Mock(
            connection=mock.Mock()
        )
        conn_rec = _frontend._ConnectionRec(
            mock_frontend,
            connection_record
        )
        conn_rec.statemachine.state = conn_rec.NOHOST

        host1 = hosts['host1'] = \
            _frontend.HostConnections(mock_frontend, raw_host1)
        host2 = hosts['host2'] = \
            _frontend.HostConnections(mock_frontend, raw_host2)

        return conn_rec, host1, host2

    def test_nohost_to_checkin_transition(self):
        conn_rec, host1, host2 = self._fixture()
        conn_rec.statemachine.state = conn_rec.NOHOST
        conn_rec.CHECKEDIN.go(conn_rec, host1)

        self.assertIn(
            conn_rec,
            host1._connection_records
        )
        self.assertEqual(conn_rec.host_name, "host1")
        self.assertEqual(host1._connections.value, 1)
        self.assertEqual(host2._connections.value, 0)

    def test_checkout_to_checkin_transition(self):
        conn_rec, host1, host2 = self._fixture()
        conn_rec.statemachine.state = conn_rec.NOHOST
        conn_rec.CHECKEDIN.go(conn_rec, host1)
        conn_rec.CHECKEDOUT.go(conn_rec, conn_rec.connection_record())

        self.assertEqual(host1._checkouts.value, 1)
        conn_rec.CHECKEDIN.go(conn_rec)
        self.assertEqual(host1._checkouts.value, 0)

        self.assertIn(
            conn_rec,
            host1._connection_records
        )
        self.assertEqual(conn_rec.host_name, "host1")
        self.assertEqual(host1._connections.value, 1)
        self.assertEqual(host2._connections.value, 0)

    def test_invalidated_checkout_to_invalidated_checkin_transition(self):
        conn_rec, host1, host2 = self._fixture()
        conn_rec.statemachine.state = conn_rec.NOHOST
        conn_rec.CHECKEDIN.go(conn_rec, host1)
        conn_rec.CHECKEDOUT.go(conn_rec, conn_rec.connection_record())

        self.assertEqual(host1._checkouts.value, 1)

        conn_rec.INVALIDATED_CHECKEDOUT.go(conn_rec)

        conn_rec.INVALIDATED_CHECKEDIN.go(conn_rec)
        self.assertEqual(host1._checkouts.value, 0)

        self.assertIn(
            conn_rec,
            host1._connection_records
        )
        self.assertEqual(conn_rec.host_name, "host1")
        self.assertEqual(host1._connections.value, 0)
        self.assertEqual(host2._connections.value, 0)

    def test_to_invalidated_transition(self):
        conn_rec, host1, host2 = self._fixture()
        conn_rec.statemachine.state = conn_rec.NOHOST
        conn_rec.CHECKEDIN.go(conn_rec, host1)
        conn_rec.INVALIDATED_CHECKEDIN.go(conn_rec)

        self.assertIn(
            conn_rec,
            host1._connection_records
        )
        self.assertEqual(conn_rec.host_name, "host1")
        self.assertEqual(host1._connections.value, 0)
        self.assertEqual(host2._connections.value, 0)

    def test_invalidated_to_nohost_transition(self):
        conn_rec, host1, host2 = self._fixture()
        conn_rec.statemachine.state = conn_rec.NOHOST
        conn_rec.CHECKEDIN.go(conn_rec, host1)
        conn_rec.INVALIDATED_CHECKEDIN.go(conn_rec)

        conn_rec.NOHOST.go(conn_rec)
        self.assertNotIn(
            conn_rec,
            host1._connection_records
        )
        self.assertEqual(conn_rec.host_name, None)
        self.assertEqual(host1._connections.value, 0)
        self.assertEqual(host2._connections.value, 0)

    def test_transition_host_from_set(self):
        conn_rec, host1, host2 = self._fixture()

        conn_rec.transition_host(host1)
        self.assertEqual("host1", conn_rec.host_name)
        listener = mock.Mock()
        conn_rec.state.add_listener(conn_rec, listener)

        conn_rec.transition_host(host2)
        self.assertEqual("host2", conn_rec.host_name)
        self.assertEqual(
            [
                mock.call(conn_rec, conn_rec.CHECKEDIN, conn_rec.NOHOST),
                mock.call(conn_rec, conn_rec.NOHOST, conn_rec.CHECKEDIN)
            ],
            listener.mock_calls
        )


class ReaperSweepTest(_ConfigFixture, unittest.TestCase):
    def _fixture(self, num_connections=1):
        config = self._config_fixture()
        cluster_config = config.clusters['rhel-cluster']
        cluster_backend = _backend.ClusterBackend.from_config(cluster_config)
        raw_host1 = cluster_backend.hosts['host1']
        raw_host2 = cluster_backend.hosts['host2']

        raw_host1.UP.go(raw_host1)
        raw_host2.UP.go(raw_host2)

        hosts = {}
        mock_frontend = mock.Mock(
            cluster_backend=cluster_backend,
            hosts=hosts,
            reap_interval=5,
            idle_timeout=120,
            max_connection_age=240
        )
        self.connection_record = connection_record = mock.Mock(
            connection=mock.Mock()
        )

        class PatchableConnRec(_frontend._ConnectionRec):
            pass

        connections = []
        for i in range(num_connections):
            conn_rec = PatchableConnRec(
                mock_frontend,
                connection_record
            )
            conn_rec.statemachine.state = conn_rec.NOHOST
            connections.append(conn_rec)

        host1 = hosts['host1'] = \
            _frontend.HostConnections(mock_frontend, raw_host1)
        host2 = hosts['host2'] = \
            _frontend.HostConnections(mock_frontend, raw_host2)

        host1._connection_records = sqla_util.OrderedSet()
        host2._connection_records = sqla_util.OrderedSet()

        for conn_rec in connections:
            conn_rec.transition_host(host1)
        return tuple(connections) + (host1, host2)

    def test_sweep_already_occurred(self):
        conn_rec, host1, host2 = self._fixture()

        host1._periodic_reaper_sweep = mock.Mock(return_value=False)
        now = 102

        with mock.patch.object(conn_rec, "check_reap") as check_reap:
            host1._reaper_sweep(now)

        self.assertEqual(
            check_reap.mock_calls,
            []
        )

    def test_sweep_checks_reap_when_due(self):
        conn_rec, host1, host2 = self._fixture()

        host1._periodic_reaper_sweep = mock.Mock(return_value=True)
        now = 108

        with mock.patch.object(
            conn_rec, "check_reap",
                mock.Mock(side_effect=conn_rec.check_reap)) as check_reap:
            host1._reaper_sweep(now)

        self.assertEqual(
            check_reap.mock_calls,
            [mock.call(108, host1._last_reaped_time)]
        )

    def test_reap_due_for_idle(self):
        conn_rec, host1, host2 = self._fixture()

        host1._periodic_reaper_sweep = mock.Mock(return_value=True)
        now = 508

        conn_rec.last_connected = 300  # not 240 secs old
        conn_rec.last_checkin = 360  # but over 120 secs idle
        with \
            mock.patch.object(
                conn_rec, "invalidate_connection_rec",
                mock.Mock(side_effect=conn_rec.invalidate_connection_rec)
            ) as invalidate_connection_rec, \
                mock.patch.object(_frontend, "log") as mock_log:
            host1._reaper_sweep(now)

        self.assertEqual(
            invalidate_connection_rec.mock_calls,
            [mock.call(soft=False)]
        )
        self.assertEqual(
            mock_log.mock_calls,
            [
                mock.call.info(
                    '%s invalidate %s connection, idle time '
                    '%s connection age %s',
                    conn_rec, 'idle', 148, 208)
            ]
        )

    def test_reap_due_for_connected(self):
        conn_rec, host1, host2 = self._fixture()

        host1._periodic_reaper_sweep = mock.Mock(return_value=True)
        now = 508

        conn_rec.last_connected = 210  # >240 secs old
        conn_rec.last_checkin = 420  # but not idle for that long
        with \
            mock.patch.object(
                conn_rec, "invalidate_connection_rec",
                mock.Mock(side_effect=conn_rec.invalidate_connection_rec)
            ) as invalidate_connection_rec, \
                mock.patch.object(_frontend, "log") as mock_log:
            host1._reaper_sweep(now)

        self.assertEqual(
            invalidate_connection_rec.mock_calls,
            [mock.call(soft=False)]
        )
        self.assertEqual(
            mock_log.mock_calls,
            [
                mock.call.info(
                    '%s invalidate %s connection, idle time '
                    '%s connection age %s',
                    conn_rec, 'old', 88, 298)
            ]
        )

    def test_reap_skips_when_reap_occured(self):
        conn_rec1, conn_rec2, host1, host2 = self._fixture(num_connections=2)

        host1._periodic_reaper_sweep = mock.Mock(return_value=True)
        now = 508

        conn_rec1.last_connected = 210  # >240 secs old
        conn_rec1.last_checkin = 420  # but not idle for that long
        conn_rec2.last_connected = 300  # not too old
        conn_rec2.last_checkin = 360  # idle
        with \
            mock.patch.object(
                conn_rec1, "invalidate_connection_rec",
                mock.Mock(side_effect=conn_rec1.invalidate_connection_rec)
            ) as invalidate_connection_rec_1, \
            mock.patch.object(
                conn_rec2, "invalidate_connection_rec",
                mock.Mock(side_effect=conn_rec2.invalidate_connection_rec)
            ) as invalidate_connection_rec_2, \
                mock.patch.object(_frontend, "log") as mock_log:
            host1._reaper_sweep(now)

            self.assertEqual(
                invalidate_connection_rec_1.mock_calls,
                [mock.call(soft=False)]
            )
            self.assertEqual(
                invalidate_connection_rec_2.mock_calls,
                []
            )
            conn_rec1.INVALIDATED_CHECKEDIN.go(conn_rec1)

            now = 518

            host1._reaper_sweep(now)
            self.assertEqual(
                invalidate_connection_rec_2.mock_calls,
                [mock.call(soft=False)]
            )

        self.assertEqual(
            mock_log.mock_calls,
            [
                mock.call.info(
                    '%s invalidate %s connection, idle time '
                    '%s connection age %s',
                    conn_rec1, 'old', 88, 298),

                # second reap didn't occur until now was 518
                mock.call.info(
                    '%s invalidate %s connection, idle time '
                    '%s connection age %s',
                    conn_rec2, 'idle', 158, 218)
            ]
        )

