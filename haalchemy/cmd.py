from zkcluster import config as cfg
from zkcluster import cmdline

from zkcluster import server

from . import service


class CmdLine(cmdline.CmdLine):
    def load_config(self):
        if self.options.config:
            self.config = cfg.Config.from_config_file(
                self.options.config, prefix="haalchemy_")
        else:
            self.config = cfg._CFG

        self.config.load_logging_configs()


class ConsoleCmd(cmdline.RAFTCmdMixin, cmdline.ConsoleCmd):
    """Set up the --console command to connect to a cluster."""

    def init_console(self, console):
        super(ConsoleCmd, self).init_console(console)
        console.speak_rpc(service.rpc_reg)

    def console_connected(self, console):
        super(ConsoleCmd, self).console_connected(console)

        server.AddMemos({"haalchemy_console": True}).send(console.rpc_service)

        console.output(
            "Connected to service '%s' (%s:%s) - use 'help' for help.",
            console.servicename, console.host, console.port)


class ServiceCmd(cmdline.RAFTCmdMixin, cmdline.ListenCmd):
    """Set up the --listen command to start a server.

    The server will have RAFT/P2P functionality.

    """
    def init_server(self, server):
        super(ServiceCmd, self).init_server(server)

        service.Service().work_for(server)


def main():
    CmdLine(
        cmds=[ServiceCmd(), ConsoleCmd()], prog="haalchemy").main()
