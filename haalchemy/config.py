import os
from sqlalchemy.engine import url

from zkcluster import config as cfg


class SAURLFixer(cfg.Fixer):
    def fix_non_none_value(self, value):
        return url.make_url(value)

cfg._CFG.prefix = "haalchemy_"
cfg._CFG.add_config_from_config_file("/etc/haalchemy.cfg")

env_cfg = os.environ.get('HAALCHEMY_CONFIG', None)
if env_cfg:
    cfg._CFG.add_config_from_config_file(env_cfg)


class CmdLineConfigMixin(object):
    def load_config(self):
        if self.options.config:
            self.config = cfg.Config.from_config_file(
                self.options.config, prefix="haalchemy_")
        else:
            self.config = cfg._CFG

        self.config.load_logging_configs()


cluster_config_section = cfg.ConfigSection(
    "cluster", [
        cfg.ConfigElement('name', default=cfg.REQUIRED),
        cfg.ConfigElement('base_url', SAURLFixer),
        cfg.ConfigElement('database_impl', default=cfg.REQUIRED),
        cfg.ConfigElement('poll_interval', cfg.IntFixer, 1),
        cfg.ConfigElement('periodic_update_interval', cfg.IntFixer, 30),
        cfg.ConfigElement('idle_timeout', cfg.IntFixer,),
        cfg.ConfigElement('max_connection_age', cfg.IntFixer),
        cfg.ConfigElement('strategy', default='default'),
        cfg.ConfigElement('reap_interval', cfg.IntFixer, 5),
        cfg.ConfigElement('hosts', cfg.HostFixer, []),
        cfg.ConfigElement('appnames', cfg.StringListFixer, []),
        cfg.ConfigElement('enabled', cfg.BoolFixer, True),
    ]
)
