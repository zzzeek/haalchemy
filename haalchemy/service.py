from sqlalchemy.engine import url
from zkcluster import rpc

from . import exc

from .cluster import cluster as _cluster
from .cluster import worker as _worker

rpc_reg = rpc.RPCReg()


class Service(object):
    def __init__(self):
        self.clients = {}

    def work_for(self, server):
        server.speak_rpc(rpc_reg)
        server.memos['ha_service'] = self
        clusters = _cluster.Cluster.get_all_clusters_from_config(
            server.config)
        self.worker = _worker.ClusterStatusWorker(clusters)
        self.clusters = self.worker.clusters
        self.worker.work_for(server)

    def _get_client(self, client_id):
        try:
            return self.clients[client_id]
        except KeyError:
            raise exc.HAException("client %s has not identified" % client_id)

    def identify(self, client_id, pid, processname, cluster_name, app_name):
        client = self.clients.get(client_id)
        if client is not None:
            # TODO: make this report an error back to the client
            assert client.pid == pid
            assert client.cluster_name == cluster_name
        else:
            client = HAClient(
                self, client_id, pid, processname, cluster_name, app_name)
            self.clients[client_id] = client

        # TODO: sync out to p2p

    def get_slots(self, client_id):
        client = self._get_client(client_id)

        # TODO: sync out to p2p

        return client.get_slots()


class HAClient(object):
    def __init__(
            self, service, client_id, pid,
            processname, cluster_name, app_name):
        self.service = service
        self.client_id = client_id
        self.pid = pid
        self.processname = processname
        self.cluster_name = cluster_name
        self.app_name = app_name

    def get_slots(self):
        # hardcode a simplistic round-robin 5 pooled / 10 overflow
        # to start.
        hosts = list(self.service.clusters[self.cluster_name].hosts.values())
        modulus = len(hosts)

        slots = [
            (hosts[i % modulus].url, True)
            for i in range(5)
        ] + [
            (hosts[i % modulus].url, False)
            for i in range(5, 15)
        ]
        return slots


rpc_reg.add_type(url.URL, "sqla_url", str, url.make_url)


class ClusterCommand(rpc.RPC):
    def receive_request(self, rpc, transport):
        service = transport.server.memos['ha_service']
        return self.process(service)


@rpc_reg.call('client_id', 'pid', 'processname', 'cluster_name', 'app_name')
class Identify(ClusterCommand):
    def process(self, service):
        return service.identify(
            self.client_id, self.pid,
            self.processname, self.cluster_name, self.app_name)


@rpc_reg.call('client_id')
class GetSlots(ClusterCommand):
    def process(self, service):
        return service.get_slots(self.client_id)


