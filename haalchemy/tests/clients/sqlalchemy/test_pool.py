import mock
import time
import unittest
import weakref

from sqlalchemy import exc as sqla_exc
from sqlalchemy import event
from sqlalchemy import pool as sqla_pool
from sqlalchemy.testing.util import gc_collect
from sqlalchemy.testing.util import lazy_gc

from haalchemy.util.compat import threading
from haalchemy.clients.sqlalchemy import pool as _pool
from haalchemy.tests import environment

join_timeout = 10

def MockDBAPI():  # noqa
    def cursor():
        return mock.Mock()

    def connect(*arg, **kw):
        return mock.Mock(cursor=mock.Mock(side_effect=cursor))

    def shutdown(value):
        if value:
            db.connect = mock.Mock(side_effect=Exception("connect failed"))
        else:
            db.connect = mock.Mock(side_effect=connect)
        db.is_shutdown = value

    db = mock.Mock(
        connect=mock.Mock(side_effect=connect),
        shutdown=shutdown,
        is_shutdown=False)
    return db


def _run_in_background(fn):
    thread = threading.Thread(target=fn)
    thread.daemon = True
    thread.start()


class PriorityPoolTest(unittest.TestCase):
    def _with_teardown(self, connection):
        self._teardown_conns.append(weakref.ref(connection))
        return connection

    def setUp(self):
        self._teardown_conns = []

    def tearDown(self):
        for ref in self._teardown_conns:
            conn = ref()
            if conn:
                conn.close()

    def _pool_w_dbapi_fixture(self, **kw):
        dbapi = MockDBAPI()
        return _pool.PriorityPool(
            creator=lambda: dbapi.connect("foo.db"),
            **kw
        ), dbapi

    def _pool_fixture(self, creator=None, **kw):
        if creator is None:
            dbapi = MockDBAPI()

            def creator():
                return dbapi.connect("foo.db")

        return _pool.PriorityPool(
            creator=creator,
            **kw
        )

    def test_get_pooled(self):
        pool, dbapi = self._pool_w_dbapi_fixture(pool_size=2, max_overflow=2)

        self.assertEqual(2, pool.size())
        self.assertEqual(0, pool.checkedout())
        self.assertEqual(0, dbapi.connect.call_count)

        conn = pool.connect()

        self.assertEqual(2, pool.size())
        self.assertEqual(1, pool.checkedout())
        self.assertEqual(1, dbapi.connect.call_count)

        conn.close()

        self.assertEqual(2, pool.size())
        self.assertEqual(0, pool.checkedout())
        self.assertEqual(1, dbapi.connect.call_count)

    def test_wait_until_available_no_overflow(self):
        pool, dbapi = self._pool_w_dbapi_fixture(pool_size=2, max_overflow=0)

        self.assertEqual(0, dbapi.connect.call_count)

        c1 = pool.connect()  # noqa
        c2 = pool.connect()

        self.assertEqual(2, dbapi.connect.call_count)

        m = mock.Mock()

        def get_c3():
            c3 = pool.connect()
            m.connect()
            c3.close()

        _run_in_background(get_c3)
        time.sleep(.1)
        self.assertEqual([], m.mock_calls)
        c2.close()
        time.sleep(.1)
        self.assertEqual([mock.call.connect()], m.mock_calls)

        self.assertEqual(2, dbapi.connect.call_count)

    # these tests are all ported from
    # sqlalchemy.engine.test_pool -> QueuePoolTest.  We are going for
    # identical behavior for PriorityPool vs. QueuePool.
    @environment.requires.predictable_gc
    def test_wait_until_available_overflow(self):
        pool, dbapi = self._pool_w_dbapi_fixture(pool_size=2, max_overflow=1)

        self.assertEqual(0, pool._total_connected)

        c1 = pool.connect()  # noqa
        self.assertEqual(1, pool._total_connected)
        c2 = pool.connect()

        self.assertEqual(2, pool._total_connected)
        self.assertEqual(2, pool.size())
        self.assertEqual(2, pool.checkedout())
        self.assertEqual(-2, pool.overflow())

        c3 = pool.connect()

        self.assertEqual(3, pool.checkedout())
        self.assertEqual(-1, pool.overflow())
        self.assertEqual(3, pool._total_connected)
        self.assertEqual(3, dbapi.connect.call_count)

        m = mock.Mock()

        c4_ret = []

        def get_c4():
            c4 = pool.connect()
            m.connect()
            c4_ret.append(c4)

        _run_in_background(get_c4)
        time.sleep(.1)
        self.assertEqual([], m.mock_calls)
        c2.close()

        time.sleep(.1)

        self.assertEqual([mock.call.connect()], m.mock_calls)

        # overflow has been used twice
        self.assertEqual(4, dbapi.connect.call_count)

        self.assertEqual(3, pool.checkedout())
        self.assertEqual(0, pool.overflow())
        self.assertEqual(3, pool._total_connected)

        c4_ret[0].close()
        self.assertEqual(2, pool.checkedout())
        self.assertEqual(-1, pool.overflow())
        self.assertEqual(2, pool._total_connected)

        c3.close()
        self.assertEqual(1, pool.checkedout())
        self.assertEqual(-2, pool.overflow())
        self.assertEqual(2, pool._total_connected)

        c1.close()
        self.assertEqual(0, pool.checkedout())
        self.assertEqual(-2, pool.overflow())
        self.assertEqual(2, pool._total_connected)

    def test_queuepool_del(self):
        self._do_testqueuepool(useclose=False)

    def test_queuepool_close(self):
        self._do_testqueuepool(useclose=True)

    def _do_testqueuepool(self, useclose=False):
        p = self._pool_fixture(
            pool_size=3,
            max_overflow=-1)

        def status(pool):
            return pool.size(), pool.checkedin(), pool.overflow(), \
                pool.checkedout()

        c1 = p.connect()
        self.assertTrue(status(p) == (3, 0, -2, 1))
        c2 = p.connect()
        self.assertTrue(status(p) == (3, 0, -1, 2))
        c3 = p.connect()
        self.assertTrue(status(p) == (3, 0, 0, 3))
        c4 = p.connect()
        self.assertTrue(status(p) == (3, 0, 1, 4))
        c5 = p.connect()
        self.assertTrue(status(p) == (3, 0, 2, 5))
        c6 = p.connect()
        self.assertTrue(status(p) == (3, 0, 3, 6))
        if useclose:
            c4.close()
            c3.close()
            c2.close()
        else:
            c4 = c3 = c2 = None
            lazy_gc()
        self.assertTrue(status(p) == (3, 3, 3, 3))
        if useclose:
            c1.close()
            c5.close()
            c6.close()
        else:
            c1 = c5 = c6 = None
            lazy_gc()
        self.assertTrue(status(p) == (3, 3, 0, 0))
        c1 = p.connect()
        c2 = p.connect()
        self.assertTrue(status(p) == (3, 1, 0, 2), status(p))
        if useclose:
            c2.close()
        else:
            c2 = None
            lazy_gc()
        self.assertTrue(status(p) == (3, 2, 0, 1))
        c1.close()
        lazy_gc()
        assert not pool._refs

    @environment.requires.timing_intensive
    def test_timeout(self):
        p = self._pool_fixture(
            pool_size=3,
            max_overflow=0,
            timeout=2)
        c1 = p.connect()  # noqa
        c2 = p.connect()  # noqa
        c3 = p.connect()  # noqa
        now = time.time()

        self.assertRaises(
            sqla_exc.TimeoutError,
            p.connect
        )
        assert int(time.time() - now) == 2

    @environment.requires.threading_with_mock
    @environment.requires.timing_intensive
    def test_timeout_race(self):
        # this tests a condition that is somewhat specific to the workings of
        # QueuePool, but here we ensure that connect attempts that
        # have to wait up front before the pool starts do get their connection
        dbapi = MockDBAPI()
        p = self._pool_fixture(
            creator=lambda: dbapi.connect(),
            pool_size=2,
            max_overflow=1, use_threadlocal=False, timeout=3)
        timeouts = []

        def checkout():
            for x in range(1):
                now = time.time()
                try:
                    c1 = p.connect()
                except sqla_exc.TimeoutError:
                    timeouts.append(time.time() - now)
                    continue
                time.sleep(4)
                c1.close()

        threads = []
        for i in range(10):
            th = threading.Thread(target=checkout)
            th.start()
            threads.append(th)
        for th in threads:
            th.join(join_timeout)

        self.assertTrue(len(timeouts) > 0, "nothing timed out")
        for t in timeouts:
            self.assertTrue(
                t >= 3, "Not all timeouts were >= 3 seconds %r" % timeouts)
            # normally, the timeout should under 4 seconds,
            # but on a loaded down buildbot it can go up.
            self.assertTrue(
                t < 14, "Not all timeouts were < 14 seconds %r" % timeouts)

    def _test_overflow(self, thread_count, max_overflow):
        gc_collect()

        dbapi = MockDBAPI()
        mutex = threading.Lock()

        def creator():
            time.sleep(.05)
            with mutex:
                return dbapi.connect()

        p = self._pool_fixture(
            creator=creator,
            pool_size=3, timeout=2,
            max_overflow=max_overflow)
        peaks = []

        def whammy():
            for i in range(10):
                try:
                    con = p.connect()
                    time.sleep(.005)
                    peaks.append(p.overflow())
                    con.close()
                    del con
                except sqla_exc.TimeoutError:
                    pass
        threads = []
        for i in range(thread_count):
            th = threading.Thread(target=whammy)
            th.start()
            threads.append(th)
        for th in threads:
            th.join(join_timeout)

        self.assertTrue(max(peaks) <= max_overflow)

        lazy_gc()
        assert not sqla_pool._refs

    def test_overflow_reset_on_failed_connect(self):
        dbapi = mock.Mock()

        def failing_dbapi():
            time.sleep(2)
            raise Exception("connection failed")

        creator = dbapi.connect

        def create():
            return creator()

        p = self._pool_fixture(creator=create, pool_size=2, max_overflow=3)
        c1 = self._with_teardown(p.connect())  # noqa
        c2 = self._with_teardown(p.connect())  # noqa
        c3 = self._with_teardown(p.connect())  # noqa
        self.assertEqual(p._overflow, 1)
        creator = failing_dbapi
        self.assertRaises(Exception, p.connect)
        self.assertEqual(p._overflow, 1)

    @environment.requires.threading_with_mock
    @environment.requires.timing_intensive
    def test_hanging_connect_within_overflow(self):
        """test that a single connect() call which is hanging
        does not block other connections from proceeding."""

        dbapi = mock.Mock()
        mutex = threading.Lock()

        def hanging_dbapi():
            time.sleep(2)
            with mutex:
                return dbapi.connect()

        def fast_dbapi():
            with mutex:
                return dbapi.connect()

        creator = threading.local()

        def create():
            return creator.mock_connector()

        def run_test(name, pool, should_hang):
            if should_hang:
                creator.mock_connector = hanging_dbapi
            else:
                creator.mock_connector = fast_dbapi

            conn = pool.connect()
            conn.operation(name)
            time.sleep(1)
            conn.close()

        p = self._pool_fixture(creator=create, pool_size=2, max_overflow=3)

        threads = [
            threading.Thread(
                target=run_test, args=("success_one", p, False)),
            threading.Thread(
                target=run_test, args=("success_two", p, False)),
            threading.Thread(
                target=run_test, args=("overflow_one", p, True)),
            threading.Thread(
                target=run_test, args=("overflow_two", p, False)),
            threading.Thread(
                target=run_test, args=("overflow_three", p, False))
        ]
        for t in threads:
            t.start()
            time.sleep(.2)

        for t in threads:
            t.join(timeout=join_timeout)
        self.assertEqual(
            dbapi.connect().operation.mock_calls,
            [mock.call("success_one"), mock.call("success_two"),
                mock.call("overflow_two"), mock.call("overflow_three"),
                mock.call("overflow_one")]
        )

    @environment.requires.threading_with_mock
    @environment.requires.timing_intensive
    def test_waiters_handled(self):
        """test that threads waiting for connections are
        handled when the pool is replaced.

        """
        mutex = threading.Lock()
        dbapi = MockDBAPI()

        def creator():
            mutex.acquire()
            try:
                return dbapi.connect()
            finally:
                mutex.release()

        success = []
        for timeout in (None, 30):
            for max_overflow in (0, -1, 3):
                p = self._pool_fixture(
                    creator=creator,
                    pool_size=2, timeout=timeout,
                    max_overflow=max_overflow)

                def waiter(p, timeout, max_overflow):
                    success_key = (timeout, max_overflow)
                    conn = p.connect()
                    success.append(success_key)
                    time.sleep(.1)
                    conn.close()

                c1 = p.connect()  # noqa
                c2 = p.connect()

                threads = []
                for i in range(2):
                    t = threading.Thread(
                        target=waiter,
                        args=(p, timeout, max_overflow))
                    t.daemon = True
                    t.start()
                    threads.append(t)

                # this sleep makes sure that the
                # two waiter threads hit upon wait()
                # inside the queue, before we invalidate the other
                # two conns
                time.sleep(.2)
                p._invalidate(c2)

                for t in threads:
                    t.join(join_timeout)

        self.assertEqual(len(success), 12, "successes: %s" % success)

    def test_connrec_invalidated_within_checkout_no_race(self):
        """Test that a concurrent ConnectionRecord.invalidate() which
        occurs after the ConnectionFairy has called
        _ConnectionRecord.checkout()
        but before the ConnectionFairy tests "fairy.connection is None"
        will not result in an InvalidRequestError.

        This use case assumes that a listener on the checkout() event
        will be raising DisconnectionError so that a reconnect attempt
        may occur.

        """
        dbapi = MockDBAPI()

        def creator():
            return dbapi.connect()

        p = self._pool_fixture(creator=creator, pool_size=1, max_overflow=0)

        conn = p.connect()
        conn.close()

        _existing_checkout = _pool._ConnectionRecord.checkout

        @classmethod
        def _decorate_existing_checkout(cls, *arg, **kw):
            fairy = _existing_checkout(*arg, **kw)
            connrec = fairy._connection_record
            connrec.invalidate()
            return fairy

        with mock.patch(
                "sqlalchemy.pool._ConnectionRecord.checkout",
                _decorate_existing_checkout):
            conn = p.connect()
            self.assertIs(conn._connection_record.connection, None)
        conn.close()

    @environment.requires.threading_with_mock
    @environment.requires.timing_intensive
    def test_notify_waiters(self):
        dbapi = MockDBAPI()

        canary = []

        def creator():
            canary.append(1)
            return dbapi.connect()
        p1 = self._pool_fixture(
            creator=creator,
            pool_size=1, timeout=None,
            max_overflow=0)

        def waiter(p):
            conn = p.connect()
            canary.append(2)
            time.sleep(.5)
            conn.close()

        c1 = p1.connect()

        threads = []
        for i in range(5):
            t = threading.Thread(target=waiter, args=(p1, ))
            t.start()
            threads.append(t)
        time.sleep(.5)
        self.assertEqual(canary, [1])

        # this also calls invalidate()
        # on c1
        p1._invalidate(c1)

        for t in threads:
            t.join(join_timeout)

        self.assertEqual(canary, [1, 1, 2, 2, 2, 2, 2])

    def test_dispose_closes_pooled_soft(self):
        dbapi = MockDBAPI()

        p = self._pool_fixture(
            creator=dbapi.connect,
            pool_size=2, timeout=None,
            max_overflow=0)
        c1 = p.connect()
        c2 = p.connect()
        c1_con = c1.connection
        c2_con = c2.connection

        c1.close()

        self.assertEqual(c1_con.close.call_count, 0)
        self.assertEqual(c2_con.close.call_count, 0)

        p.dispose()

        # QueuePool leaves one checked out connection
        # opened, PriorityPool at the moment emulates
        # this by default, so c2_con.close not called yet
        self.assertEqual(c1_con.close.call_count, 1)
        self.assertEqual(c2_con.close.call_count, 0)

        # if a ConnectionFairy is closed
        # after the pool has been disposed, there's no
        # flag that states it should be invalidated
        # immediately - it just gets returned to the
        # pool normally.  this is QueuePool behavior too.
        c2.close()
        self.assertEqual(c1_con.close.call_count, 1)
        self.assertEqual(c2_con.close.call_count, 0)

        # .. and that's the one we should get back
        c3 = p.connect()
        assert c3.connection is c2_con

    def test_dispose_closes_pooled_hard(self):
        dbapi = MockDBAPI()

        p = self._pool_fixture(
            creator=dbapi.connect,
            pool_size=2, timeout=None,
            max_overflow=0)
        c1 = p.connect()
        c2 = p.connect()
        c1_con = c1.connection
        c2_con = c2.connection

        c1.close()

        self.assertEqual(c1_con.close.call_count, 0)
        self.assertEqual(c2_con.close.call_count, 0)

        p.dispose(hard=True)

        # hard flag means it closes checked out connection
        # also, so close() called for both
        self.assertEqual(c1_con.close.call_count, 1)
        self.assertEqual(c2_con.close.call_count, 1)

        # connection was already closed
        c2.close()
        self.assertEqual(c1_con.close.call_count, 1)
        self.assertEqual(c2_con.close.call_count, 1)

        # new connction when we start up again
        c3 = p.connect()
        assert c3.connection is not c2_con

    @environment.requires.threading_with_mock
    @environment.requires.timing_intensive
    def test_no_overflow(self):
        self._test_overflow(40, 0)

    @environment.requires.threading_with_mock
    @environment.requires.timing_intensive
    def test_max_overflow(self):
        self._test_overflow(40, 5)

    def test_mixed_close(self):
        sqla_pool._refs.clear()
        p = self._pool_fixture(
            pool_size=3, max_overflow=-1,
            use_threadlocal=True)
        c1 = p.connect()
        c2 = p.connect()
        assert c1 is c2
        c1.close()
        c2 = None
        assert p.checkedout() == 1
        c1 = None
        lazy_gc()
        assert p.checkedout() == 0
        lazy_gc()
        assert not sqla_pool._refs

    def test_overflow_no_gc_tlocal(self):
        self._test_overflow_no_gc(True)

    def test_overflow_no_gc(self):
        self._test_overflow_no_gc(False)

    def _test_overflow_no_gc(self, threadlocal):
        p = self._pool_fixture(
            pool_size=2,
            max_overflow=2)

        # disable weakref collection of the
        # underlying connections
        strong_refs = set()

        def _conn():
            c = p.connect()
            strong_refs.add(c.connection)
            return c

        for j in range(5):
            # open 4 conns at a time.  each time this
            # will yield two pooled connections + two
            # overflow connections.
            conns = [_conn() for i in range(4)]
            for c in conns:
                c.close()

        # doing that for a total of 5 times yields
        # ten overflow connections closed plus the
        # two pooled connections unclosed.

        self.assertEqual(
            set([c.close.call_count for c in strong_refs]),
            set([1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0])
        )

    @environment.requires.predictable_gc
    def test_weakref_kaboom(self):
        p = self._pool_fixture(
            pool_size=3,
            max_overflow=-1, use_threadlocal=True)
        c1 = p.connect()
        c2 = p.connect()
        c1.close()
        c2 = None
        del c1
        del c2
        gc_collect()
        assert p.checkedout() == 0
        c3 = p.connect()
        assert c3 is not None

    def test_trick_the_counter(self):
        """this is a "flaw" in the connection pool; since threadlocal
        uses a single ConnectionFairy per thread with an open/close
        counter, you can fool the counter into giving you a
        ConnectionFairy with an ambiguous counter.  i.e. its not true
        reference counting."""

        p = self._pool_fixture(
            pool_size=3,
            max_overflow=-1, use_threadlocal=True)
        c1 = p.connect()
        c2 = p.connect()
        assert c1 is c2
        c1.close()
        c2 = p.connect()
        c2.close()
        self.assertTrue(p.checkedout() != 0)
        c2.close()
        self.assertTrue(p.checkedout() == 0)

    def test_recycle(self):
        with mock.patch("sqlalchemy.pool.time.time") as mock:
            mock.return_value = 10000

            p = self._pool_fixture(
                pool_size=1,
                max_overflow=0,
                recycle=30)
            c1 = p.connect()
            c_ref = weakref.ref(c1.connection)
            c1.close()
            mock.return_value = 10001
            c2 = p.connect()

            self.assertIs(c2.connection, c_ref())
            c2.close()

            mock.return_value = 10035
            c3 = p.connect()
            self.assertIsNot(c3.connection, c_ref())

    @environment.requires.timing_intensive
    def test_recycle_on_invalidate(self):
        p = self._pool_fixture(
            pool_size=1,
            max_overflow=0)
        c1 = p.connect()
        c_ref = weakref.ref(c1.connection)
        c1.close()
        c2 = p.connect()
        self.assertIs(c2.connection, c_ref())

        c2_rec = c2._connection_record
        p._invalidate(c2)
        assert c2_rec.connection is None
        c2.close()
        time.sleep(.5)
        c3 = p.connect()

        self.assertIsNot(c3.connection, c_ref())

    @environment.requires.timing_intensive
    def test_recycle_on_soft_invalidate(self):
        p = self._pool_fixture(
            pool_size=1,
            max_overflow=0)
        c1 = p.connect()
        c_ref = weakref.ref(c1.connection)
        c1.close()
        c2 = p.connect()
        self.assertIs(c2.connection, c_ref())

        c2_rec = c2._connection_record
        c2.invalidate(soft=True)
        self.assertIs(c2_rec.connection, c2.connection)

        c2.close()
        time.sleep(.5)
        c3 = p.connect()
        self.assertIsNot(c3.connection, c_ref())
        self.assertIs(c3._connection_record, c2_rec)
        self.assertIs(c2_rec.connection, c3.connection)

    def _no_wr_finalize(self):
        finalize_fairy = sqla_pool._finalize_fairy

        def assert_no_wr_callback(
            connection, connection_record,
                pool, ref, echo, fairy=None):
            if fairy is None:
                raise AssertionError(
                    "finalize fairy was called as a weakref callback")
            return finalize_fairy(
                connection, connection_record, pool, ref, echo, fairy)
        return mock.patch.object(
            sqla_pool, '_finalize_fairy', assert_no_wr_callback)

    def _assert_cleanup_on_pooled_reconnect(self, dbapi, p):
        # p is PriorityPool with size=1, max_overflow=2,
        # and one connection in the pool that will need to
        # reconnect when next used (either due to recycle or invalidate)

        with self._no_wr_finalize():
            self.assertEqual(p.checkedout(), 0)
            self.assertEqual(p.overflow(), 0)
            dbapi.shutdown(True)
            self.assertRaises(
                Exception,
                p.connect
            )
            self.assertEqual(p.overflow(), 0)
            self.assertEqual(p.checkedout(), 0)  # and not 1

            dbapi.shutdown(False)

            c1 = self._with_teardown(p.connect())  # noqa
            self.assertEqual(p.checkedin(), 0)
            c2 = self._with_teardown(p.connect())  # noqa
            self.assertEqual(p.overflow(), 1)  # and not 2

            # this hangs if p._overflow is 2
            c3 = self._with_teardown(p.connect())

            c3.close()

    def test_error_on_pooled_reconnect_cleanup_invalidate(self):
        p, dbapi = self._pool_w_dbapi_fixture(pool_size=1, max_overflow=2)
        c1 = p.connect()
        c1.invalidate()
        c1.close()
        self._assert_cleanup_on_pooled_reconnect(dbapi, p)

    @environment.requires.timing_intensive
    def test_error_on_pooled_reconnect_cleanup_recycle(self):
        p, dbapi = self._pool_w_dbapi_fixture(
            pool_size=1,
            max_overflow=2, recycle=1)
        c1 = p.connect()
        c1.close()
        time.sleep(1.5)
        self._assert_cleanup_on_pooled_reconnect(dbapi, p)

    def test_error_on_pooled_reconnect_cleanup_wcheckout_event(self):
        p, dbapi = self._pool_w_dbapi_fixture(
            pool_size=1,
            max_overflow=2)

        c1 = p.connect()
        c1.close()

        self.assertEqual(p._total_connected, 1)

        @event.listens_for(p, "checkout")
        def handle_checkout_event(dbapi_con, con_record, con_proxy):
            if dbapi.is_shutdown:
                raise sqla_exc.DisconnectionError()

        self._assert_cleanup_on_pooled_reconnect(dbapi, p)

    def test_invalidate(self):
        p = self._pool_fixture(pool_size=1, max_overflow=0)
        c1 = p.connect()
        c_id = c1.connection.id
        c1.close()
        c1 = None
        c1 = p.connect()
        assert c1.connection.id == c_id
        c1.invalidate()
        c1 = None
        c1 = p.connect()
        assert c1.connection.id != c_id

    def test_recreate(self):
        p = self._pool_fixture(
            reset_on_return=None, pool_size=1,
            max_overflow=0)
        p2 = p.recreate()
        assert p2.size() == 1
        assert p2._reset_on_return is pool.reset_none
        assert p2._use_threadlocal is False
        assert p2._max_overflow == 0

    def test_reconnect(self):
        """tests reconnect operations at the pool level.  SA's
        engine/dialect includes another layer of reconnect support for
        'database was lost' errors."""

        p, dbapi = self._pool_w_dbapi_fixture(pool_size=1, max_overflow=0)
        c1 = p.connect()
        c_id = c1.connection.id
        c1.close()
        c1 = None
        c1 = p.connect()
        assert c1.connection.id == c_id
        dbapi.raise_error = True
        c1.invalidate()
        c1 = None
        c1 = p.connect()
        assert c1.connection.id != c_id

    def test_detach(self):
        p, dbapi = self._pool_w_dbapi_fixture(pool_size=1, max_overflow=0)
        c1 = p.connect()
        c1.detach()
        c2 = p.connect()  # noqa
        self.assertEqual(
            dbapi.connect.mock_calls,
            [mock.call("foo.db"), mock.call("foo.db")])

        c1_con = c1.connection
        assert c1_con is not None
        self.assertEqual(c1_con.close.call_count, 0)
        c1.close()
        self.assertEqual(c1_con.close.call_count, 1)

    def test_detach_via_invalidate(self):
        p, dbapi = self._pool_w_dbapi_fixture(pool_size=1, max_overflow=0)

        c1 = p.connect()
        c1_con = c1.connection
        c1.invalidate()
        assert c1.connection is None
        self.assertEqual(c1_con.close.call_count, 1)

        c2 = p.connect()
        assert c2.connection is not c1_con
        c2_con = c2.connection

        c2.close()
        self.assertEqual(c2_con.close.call_count, 0)

    def test_threadfairy(self):
        p = self._pool_fixture(pool_size=3, max_overflow=-1,
                                    use_threadlocal=True)
        c1 = p.connect()
        c1.close()
        c2 = p.connect()
        assert c2.connection is not None

