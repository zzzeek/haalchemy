import copy
import datetime
import logging
import time

from zkcluster.state import StateMachine
from zkcluster import util as zkutil

from .. import config as cfg
from . import database
from .. import exc


log = logging.getLogger(__name__)


class Cluster(object):

    def __init__(self, config, cluster_name):
        self.config = config
        self.cluster_config = self.get_cluster_config(config, cluster_name)
        self.cluster_name = cluster_name

        self.base_url = self.cluster_config['base_url']
        self.database_impl = database.DBImpl.get_impl(
            self.cluster_config['database_impl'])
        self.database_impl.validate(self.base_url)

        dialect_cls = self.base_url.get_dialect()
        dbapi = dialect_cls.dbapi()
        self.dialect = dialect_cls(dbapi=dbapi)

        self.poll_interval = self.cluster_config['poll_interval']
        self.periodic_update_interval = \
            self.cluster_config['periodic_update_interval']
        self._available = []
        self.hosts = {}

        for host in self.cluster_config['hosts']:
            hostname = host['hostname']
            port = host['port']
            name = host.get('name', hostname)
            self.hosts[name] = Host(self, name, hostname, port)

    @property
    def is_enabled(self):
        return self.cluster_config['enabled']

    @classmethod
    def get_cluster_config(cls, config, cluster_name):
        return cfg.cluster_config_section.from_config(config, cluster_name)

    @classmethod
    def get_all_clusters_from_config(cls, config):
        return [
            Cluster(config, cluster_name)
            for cluster_name in
            cfg.cluster_config_section.get_all_section_names(config)
        ]


class Host(object):
    state = StateMachine()
    START = state.START
    UP = state.new("UP")
    DOWN = state.new("DOWN")

    state.null(DOWN, DOWN)
    state.null(UP, UP)

    @state.transition(UP, DOWN)
    @state.transition(START, DOWN)
    def _set_down(self, from_, to, status=None):
        if status is not None:
            log.info("Cluster %s Host %s is DOWN",
                     self.cluster.cluster_name, self.name)
            if from_ is self.UP:
                self.cluster._available.remove(self.name)
        self.up = False

    @state.transition(DOWN, UP)
    @state.transition(START, UP)
    def _set_up(self, from_, to, status=None):
        if status is not None:
            log.info("Cluster %s Host %s is UP",
                     self.cluster.cluster_name, self.name)
            self.cluster._available.append(self.name)
            self.up_since = time.time()
            self._error_count = 0
        self.up = True

    def __init__(self, cluster, name, hostname, port):
        self.cluster = cluster
        self.name = name
        self.hostname = hostname
        self.port = port

        self.url = copy.copy(self.cluster.base_url)
        self.url.host = self.hostname
        self.url.port = self.port

        self._server_connections = -1
        self._error_count = 0
        self.up_since = None
        self.up = False
        self.state.init_instance(self)

    def inc_error_count(self):
        """increment the error count, indicating a disconnect situation
        has occurred on this host.

        TODO: this isn't being called right now.

        """
        self._error_count += 1

    def hostchecker_set_status(self, status):
        self._server_connections = status.connection_count
        if status.up and not self.UP.current(self):
            self.UP.go(self, status)
        elif not status.up and not self.DOWN.current(self):
            self.DOWN.go(self, status)

    @property
    def position(self):
        """our position in the 'available' list."""

        if self.up:
            try:
                return self.cluster._available.index(self.name)
            except ValueError:
                return -1
        else:
            return -1

    @property
    def up_since_isoformat(self):
        if not self.up_since:
            return "N/A"
        else:
            return datetime.datetime.fromtimestamp(self.up_since).isoformat()

    @property
    def server_connections(self):
        """return the current estimated count of connections on this host
        across all applications, e.g. server-side host count.

        """
        return self._server_connections

    @property
    def error_count(self):
        """Return the current count of errors logged via the
        :meth:`.inc_error_count` method.

        """
        return self._error_count


class HostChecker(object):

    def __init__(self, host):
        self.cluster = host.cluster
        self.host = host
        self.name = host.name
        self.database_impl = self.cluster.database_impl
        self.dialect = self.cluster.dialect
        self.url = host.url

        self.cargs, self.cparams = self.cluster.\
            dialect.create_connect_args(self.url)
        self._check_connection = None
        self._host_check = \
            zkutil.periodic_timer(self.cluster.poll_interval)

    def connect(self, raise_plain=True):
        return self.database_impl.connect(self, raise_plain=raise_plain)

    def _needs_check(self, now):
        if self.host.UP.current(self.host) and self.host._error_count > 0:
            return True
        else:
            return self._host_check(now)

    def disconnect(self):
        """Disconnect local DB connections when we are not performing
        checks ourselves."""

        if self._check_connection is None:
            return
        try:
            self.database_impl.close(self, self._check_connection)
        except:
            log.info("Error occurred closing ping connection", exc_info=True)
        finally:
            self._check_connection = None

    def check_status(self, now, force=False):
        """set up/down status by hitting the database directly."""
        if not force and not self._needs_check(now):
            return

        if not self._check_connection:
            try:
                self._check_connection = self.connect(raise_plain=False)
            except exc.ConnectionFailed:
                self._check_connection = None
                self._set_status(
                    database.Status.result(self, False, -1, now))
                return

        try:
            result = self.database_impl.check(
                self, self._check_connection, now)
        except exc.OperationFailed:
            if self._check_connection:
                try:
                    self.database_impl.close(self, self._check_connection)
                except exc.OperationFailed:
                    pass
                self._check_connection = None
            self._set_status(database.Status.result(self, False, -1, now))
        else:
            self._set_status(result)

    def _set_status(self, status):
        self.host.hostchecker_set_status(status)


