import logging
import time

from zkcluster import worker
from zkcluster.util import async as zkasync

from . import cluster as _cluster
from . import database
from ..util import compat

log = logging.getLogger(__name__)


class ClusterStatusWorker(worker.LeaderWorker):
    """Do some work server side, but only if we are the raft "leader".

    Otherwise, receive data from whoever the "leader" is for the same
    work.


    """
    def __init__(self, clusters):
        self.threaded_suite = zkasync.AsyncSuite.get(green=False)
        self.queue = self.threaded_suite.queue()

        self.clusters = {}
        self.host_checkers = {}

        for cluster in clusters:
            self.clusters[cluster.cluster_name] = cluster
            for host in cluster.hosts.values():
                self.host_checkers[(cluster.cluster_name, host.name)] = \
                    QueuedHostChecker(host, self.threaded_suite, self.queue)

    @worker.LeaderWorker.receiver
    def hostchecker_set_status(self, database_status, _remote, **kw):

        # database_status received as a python list for remote
        if _remote:
            database_status = database.Status(*database_status)

        log.debug("Database status: %s", database_status)

        host = self.clusters[database_status.cluster_name].\
            hosts[database_status.host_name]
        host.hostchecker_set_status(database_status)

    def work(self):
        while self.keep_working:
            print("starting work")

            for checker in self.host_checkers.values():
                if checker.cluster.is_enabled:
                    checker.start_checks()

            while self.continue_work:
                # here, we are running in a greenlet and fetching from
                # a threaded queue.   this is the link between an OS level
                # thread used for DB checking and the greenlets used for
                # everything else.
                try:
                    status_message = self.queue.get_nowait()
                except compat.queue.Empty:
                    self.async_suite.sleep(.1)
                else:
                    self.hostchecker_set_status(status_message)

            print("stopping work")
            for checker in self.host_checkers.values():
                if checker.cluster.is_enabled:
                    checker.shutdown()


class QueuedHostChecker(_cluster.HostChecker):
    def __init__(self, host, async_suite, queue):
        super(QueuedHostChecker, self).__init__(host)
        self.async_suite = async_suite
        self.queue = queue
        self._keep_working = False

    def start_checks(self):
        self._keep_working = True
        self.thread = self.async_suite.background_thread(self._work)

    def shutdown(self):
        self._keep_working = False

    def _work(self):
        force = True
        while self._keep_working:
            now = time.time()
            self.check_status(now, force)
            force = False
            self.async_suite.sleep(.05)
        self.disconnect()

    def _set_status(self, status):
        self.queue.put(status)
