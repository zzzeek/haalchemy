
import logging
import collections
from .. import exc
import re

log = logging.getLogger(__name__)


class Status(collections.namedtuple(
        "Status", [
            "cluster_name", "host_name", "up",
            "connection_count", "timestamp"])):

    @classmethod
    def result(cls, host, updown, connection_count, now):
        return Status(
            host.cluster.cluster_name, host.name,
            bool(updown), connection_count, now)


class DBImpl(object):
    _impls = {}

    @classmethod
    def for_(cls, name, driver_re):
        def decorate(klass):
            cls._impls[name] = klass
            klass.driver_re = driver_re
            return klass
        return decorate

    @classmethod
    def get_impl(cls, name):
        try:
            return cls._impls[name]()
        except KeyError:
            raise exc.ConfigurationException("no such database impl %r" % name)


@DBImpl.for_("plain", ".*")
class GenericDBImpl(DBImpl):

    def validate(self, url):
        if not re.match(self.driver_re, url.drivername):
            raise exc.ConfigurationException(
                "HAAlchemy database_impl '%s' is not compatible "
                "with database backend/driver '%s'" %
                (self.name, url.drivername))

    def connect_args(self, host):
        return host.cargs, host.cparams

    def connect(self, host, raise_plain=True):
        try:
            cargs, cparams = self.connect_args(host)
            log.debug("Connecting to %s", host.url)
            conn = host.dialect.connect(*cargs, **cparams)
        except host.dialect.dbapi.Error as oe:
            if raise_plain:
                log.info(
                    "Host %s exception on connect: %s",
                    host.name, oe)
                raise
            else:
                log.debug(
                    "Host %s check impl connection failed: %s",
                    host.name, oe)
                raise exc.ConnectionFailed()
        else:
            return conn

    def close(self, host, dbapi_connection):
        try:
            log.debug("Disconnecting %s", host.url)
            dbapi_connection.close()
        except self.dialect.dbapi.Error:
            log.error("check impl close failed", exc_info=True)
            raise exc.OperationFailed()

    def check(self, host, dbapi_connection, now):
        try:
            log.debug("Pinging host %s", host.url)
            cursor = dbapi_connection.cursor()
            cursor.execute("select 1")
            cursor.fetchone()
            cursor.close()
        except host.dialect.dbapi.Error:
            log.debug("check failed", exc_info=True)
            raise exc.OperationFailed()
        else:
            return Status.result(host, True, -1, now)


@DBImpl.for_("postgresql", "^postgresql\+?")
class PostgresqlDBImpl(GenericDBImpl):

    def check(self, host, dbapi_connection, now):
        try:
            log.debug("Pinging host %s", host.url)
            cursor = dbapi_connection.cursor()
            cursor.execute("select count(1) from pg_stat_activity")
            result = cursor.fetchone()
            cursor.close()
        except host.dialect.dbapi.Error:
            log.debug("check failed", exc_info=True)
            raise exc.OperationFailed()
        else:
            return Status.result(host, True, result[0], now)


@DBImpl.for_("mysql", "^mysql\+?")
class MySQLDBImpl(GenericDBImpl):

    def connect_args(self, host):
        cparams = host.cparams.copy()
        if 'connect_timeout' in cparams:
            log.warn(
                "Overriding given connect timeout setting of %s with 2",
                cparams['connect_timeout'])
        cparams['connect_timeout'] = 2
        return host.cargs, cparams

    def check(self, host, dbapi_connection, now):
        try:
            log.debug("Pinging host %s", host.url)
            cursor = dbapi_connection.cursor()
            cursor.execute(
                "show status where variable_name = 'Threads_connected'")
            values = dict(row for row in cursor.fetchall())
            cursor.close()
        except host.dialect.dbapi.Error:
            log.debug("check failed", exc_info=True)
            raise exc.OperationFailed()
        else:
            return Status.result(
                host, True, int(values['Threads_connected']), now
            )


@DBImpl.for_("galera", "^mysql\+?")
class GaleraDBImpl(MySQLDBImpl):

    def check(self, host, dbapi_connection, now):
        try:
            log.debug("Checking Galera status %s", host.url)
            cursor = dbapi_connection.cursor()
            cursor.execute(
                "show status where variable_name in "
                "('Threads_connected', 'wsrep_local_state')")
            values = dict(row for row in cursor.fetchall())
            cursor.close()
        except host.dialect.dbapi.Error:
            log.debug("check failed", exc_info=True)
            raise exc.OperationFailed()
        else:
            return Status.result(
                host,
                str(values['wsrep_local_state']) == '4',
                int(values['Threads_connected']), now
            )
