"""Host selection strategies."""

import collections
import random

from . import exc


class HostSelectionStrategy(object):
    _strategies = {}

    def __init__(self, config, cluster_backend):
        self.config = config
        self.cluster_backend = cluster_backend
        self.appname = config.get('appname', None)

    @classmethod
    def strategy(cls, name):
        def decorate(klass):
            cls._strategies[name] = klass
            return klass
        return decorate

    def get_host(self):
        names = self.cluster_backend.get_available()
        host_name = self._select_name(names)
        return self.cluster_backend.hosts[host_name]

    def _select_name(self, names):
        raise NotImplementedError()


@HostSelectionStrategy.strategy("leastconn")
class LeastConnSelectionStrategy(HostSelectionStrategy):
    """Server with least connections strategy.

    """

    def _select_name(self, names):

        counts = collections.defaultdict(list)
        for name in names:
            host = self.cluster_backend.hosts[name]
            host_count = host.server_connections
            if host_count > -1:
                counts[host_count].append(name)

        if not counts:
            raise exc.NoHostsAvailableException()

        lowest = counts[min(counts)]

        return random.choice(lowest)


@HostSelectionStrategy.strategy("roundrobin")
class RoundRobinSelectionStrategy(HostSelectionStrategy):

    def __init__(self, config, cluster_backend):
        super(RoundRobinSelectionStrategy, self).__init__(
            config, cluster_backend)
        self._counter = cluster_backend._counter()

    def _select_name(self, names):
        if not names:
            raise exc.NoHostsAvailableException()
        # TODO: this would at least have to coordinate among subprocesses.
        with self._counter.get_lock():
            if self._counter.value > len(names):
                self._counter.value = 0
            name = names[self._counter.value]
            self._counter.value += 1
        return name


@HostSelectionStrategy.strategy("single")
class SingleSelectionStrategy(HostSelectionStrategy):
    def _select_name(self, names):
        try:
            return names[0]
        except IndexError:
            raise exc.NoHostsAvailableException()


@HostSelectionStrategy.strategy("single-perapp")
class SinglePerAppSelectionStrategy(HostSelectionStrategy):

    def __init__(self, config, cluster_backend):
        super(SinglePerAppSelectionStrategy, self).__init__(
            config, cluster_backend)

        try:
            appnames = config["appnames"]
        except KeyError:
            raise exc.ConfigurationException(
                "single-perapp strategy requires "
                "the 'appnames' parameter is specified"
            )
        else:
            try:
                self.hash_ = appnames.index(self.appname)
            except ValueError:
                raise exc.ConfigurationException(
                    "No appname '%s' is specified" % self.appname)

    def _select_name(self, names):
        try:
            return names[self.hash_ % len(names)]
        except IndexError:
            raise exc.NoHostsAvailableException()


def _default_strategy(config, cluster_backend):
    if len(cluster_backend.hosts) == 1:
        return SingleSelectionStrategy(config, cluster_backend)
    else:
        return LeastConnSelectionStrategy(config, cluster_backend)
