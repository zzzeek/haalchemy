import re

from . import pool
from .. import client as _client
from sqlalchemy.engine import CreateEnginePlugin

# mysql+pymsql://scott:tiger@myhost1,myhost2,myhost3:4567/rhel-cluster/nova?plugin=haalchemy
# mysql+pymsql://scott:tiger@[myhost1:4567,myhost2:4567,myhost3:4567]/rhel-cluster/nova?plugin=haalchemy
#
#
#
#


class HAPlugin(CreateEnginePlugin):
    def __init__(self, url, kwargs):
        self.url = url
        self.kwargs = kwargs
        assert 'pool' not in kwargs
        kwargs['poolclass'] = pool.HAClientPool

    def handle_pool_kwargs(self, pool_cls, pool_args):
        """parse and modify pool kwargs"""

        url = self.url

        clustername, appname = self._parse_dbname(url)

        client = _client.HAClient(
            url.username,
            url.password,
            self._parse_host(url),
            clustername,
            appname
        )
        pool_args['client'] = client

    def _parse_dbname(self, url):
        tokens = url.database.split("/", 1)
        clustername = tokens.pop(0)
        if tokens:
            appname = tokens[0]
        else:
            appname = None
        return clustername, appname

    def _parse_host(self, url):
        hostnames = re.split(r",", url.host)

        default_port = url.port or 5084
        hosts = []
        for host in hostnames:
            if ':' in host:
                host, port = re.split(":", host)
                port = int(port)
            else:
                port = default_port
            hosts.append((host, port))
        return hosts

