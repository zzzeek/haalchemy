import collections
import heapq
import os
from time import time as _time
import weakref

from sqlalchemy.pool import Pool, _ConnectionRecord
from sqlalchemy import exc as sqla_exc
from sqlalchemy import event
from sqlalchemy.engine import url as _url

from ...util import compat


class AbstractPriorityPool(Pool):
    """A :class:`sqlalchemy.pool.Pool` that stores a fixed number of
    persistent connections along with a fixed set of "overflow" slots.

    :class:`.AbstractPriorityPool` is an improved version of the SQLAlchemy
    :class:`sqlalchemy.pool.QueuePool` class, with simpler operation
    and the ability to be dynamically resized as well as to support
    even distribution of connections that are across multiple hosts.

    The Queue implementation is a small subset of the stdlib Queue.Queue
    with the additional heapq use of Queue.PriorityQueue.

    """

    def __init__(self, creator, timeout=None, **kw):
        Pool.__init__(self, creator, **kw)
        self._all_slots = []
        self._queue = []
        self._returned_queue = collections.deque()
        self._mutex = compat.threading.Lock()
        self._not_empty = compat.threading.Condition(self._mutex)
        self._timeout = timeout
        self._overflow_count = 0
        self._thread_id = None

    def _do_get(self):
        self._not_empty.acquire()
        try:
            if self._timeout is None:
                while not self._queue:
                    self._not_empty.wait()
            else:
                endtime = _time() + self._timeout
                while not self._queue:
                    remaining = endtime - _time()
                    if remaining <= 0.0:
                        raise sqla_exc.TimeoutError(
                            "Connection pool is exhausted of connections, "
                            "timeout of %d seconds reached" %
                            (self._timeout))
                    self._not_empty.wait(remaining)
            heap_rec = heapq.heappop(self._queue)
            if heap_rec.is_overflow:
                self._overflow_count += 1
            return heap_rec.connection_record
        finally:
            self._not_empty.release()

    def _do_return_conn(self, connection_record):
        # there's a minute chance this block can be reentrant,
        # if a sqlalchemy ConnectionFairy in the application is garbage
        # collected w/o an explicit close() as we are modifying the _queue.
        # sqlalchemy QueuePool handles this by using an RLock. Here,
        # we support reentrancy explicitly by storing thread ident
        # and adding reentrant calls to another work queue.

        returned_queue = self._returned_queue
        me = compat.thread.get_ident()

        if self._thread_id == me:
            # reentrant call.  add rec to the "to be returned" queue,
            # leave w/o locking.
            returned_queue.append(connection_record)
            return

        self._mutex.acquire()
        try:
            self._thread_id = me
            returned_queue.append(connection_record)
            while returned_queue:
                rec = returned_queue.pop()
                wrapper = rec.record_info["slot"]()
                if self._overflow_count:
                    self._overflow_count -= 1
                    wrapper.checkin_for_overflow()
                else:
                    wrapper.checkin_for_pooled()
                heapq.heappush(self._queue, wrapper)
                self._not_empty.notify()
        finally:
            # huge assumption.  That gc collection will *never* be called
            # in this thread in between these two calls.  SQLA's QueuePool
            # essentially makes a similar assumption within RLock.
            self._thread_id = None
            self._mutex.release()

    def _queue_slot(self, pooled, url=None):
        # pre allocate ConnectionRecords, but leave them unconnected.
        connection_record = _ConnectionRecord(self, connect=False)
        slot_rec = _HeapWrapper(connection_record, pooled, url)
        self._all_slots.append(slot_rec)
        heapq.heappush(self._queue, slot_rec)

    def size(self):
        return len([rec for rec in self._all_slots if rec.is_pooled])

    def checkedin(self):
        return len([rec for rec in self._queue if rec.is_pooled])

    def overflow(self):
        """this attribute emulates QueuePool's notion of 'overflow'"""

        pooled_never_connected = len([
            rec for rec in self._all_slots
            if rec.is_pooled and not rec.was_ever_connected
        ])
        return 0 - (pooled_never_connected - self._overflow_count)

    def checkedout(self):
        return len(self._all_slots) - len(self._queue)

    @property
    def _total_connected(self):
        return len([rec for rec in self._all_slots if rec.is_connected])

    def dispose(self, hard=False):
        for rec in self._all_slots:
            if hard or not rec.in_use:
                rec.connection_record.close()
        self._queue.sort()

    def _empty(self):
        self._all_slots = []
        self._queue = []


class PriorityPool(AbstractPriorityPool):
    """Basic implementation of AbstractPriorityPool that provides the same
    configuration as QueuePool.

    The main purpose of this pool is to allow tests designed for QueuePool
    to be runnable against AbstractPriorityPool to verify its operation.

    """
    def __init__(self, creator, pool_size=5, max_overflow=10, timeout=30,
                 **kw):
        """
        Construct a PriorityPool.

        :param creator: a callable function that returns a DB-API
          connection object, same as that of :paramref:`.Pool.creator`.

        :param pool_size: Number of persistent connections to be
          maintained, defaults to 5.   A size of zero means no
          connections will remain persistent.

        :param max_overflow: Number of additional non-persistent connection
          slots to maintain, defaults to 10.

        :param timeout: The number of seconds to wait before giving up
          on returning a connection. Defaults to 30.

        :param \**kw: Other keyword arguments including
          :paramref:`.Pool.recycle`, :paramref:`.Pool.echo`,
          :paramref:`.Pool.reset_on_return` and others are passed to the
          :class:`.Pool` constructor.

        """
        super(PriorityPool, self).__init__(creator, timeout=timeout, **kw)
        self.pool_size = pool_size
        self.max_overflow = max_overflow

        self._fill_queue()

    def _fill_queue(self):
        self._mutex.acquire()
        try:
            for i in compat.range(self.pool_size):
                self._queue_slot(True)
            for i in compat.range(self.max_overflow):
                self._queue_slot(False)
        finally:
            self._mutex.release()


class HAClientPool(AbstractPriorityPool):
    def __init__(self, creator, client, **kw):
        super(HAClientPool, self).__init__(creator, **kw)
        self.client = client
        self.pid = None
        self._connect_args = {}
        event.listen(self._dialect, 'do_connect', self._create_connection)

    def _create_connection(self, dialect, connection_record, cargs, cparams):
        url = connection_record.record_info['url']
        try:
            new_cargs, new_cparams = self._connect_args[url]
        except KeyError:
            self._connect_args[url] = new_cargs, new_cparams = \
                self._dialect.create_connect_args(url)

        cargs[:] = new_cargs
        cparams.update(new_cparams)

    def _init_for_pid(self):
        pid = os.getpid()
        if pid != self.pid:
            self._mutex.acquire()
            try:
                if pid == self.pid:
                    return

                self._empty()
                self.client.connect_persistent()

                for url, pooled in self.client.get_slots():
                    self._queue_slot(pooled, url)

            finally:
                self._mutex.release()

    def _do_get(self):
        self._init_for_pid()
        return super(HAClientPool, self)._do_get()


class _HeapWrapper(object):
    """Wrapper for _ConnectionRecord that supports sorting by connection
    type."""

    __slots__ = 'connection_record', 'pooled', '__weakref__'

    def __init__(self, connection_record, pooled, url=None):
        self.connection_record = connection_record
        self.pooled = pooled
        connection_record.record_info["slot"] = weakref.ref(self)
        if url:
            connection_record.record_info["url"] = url

    @property
    def rank(self):
        return (1 if not self.pooled else 0) + (0 if self.is_connected else 1)

    def __lt__(self, other):
        return self.rank < other.rank

    @property
    def is_pooled(self):
        return self.pooled

    @property
    def is_overflow(self):
        return not self.pooled

    @property
    def is_connected(self):
        return self.connection_record.connection is not None

    @property
    def was_ever_connected(self):
        return self.connection_record.last_connect_time is not None

    @property
    def in_use(self):
        return self.connection_record.in_use

    def checkin_for_overflow(self):
        self.pooled = False
        self.connection_record.close()

    def checkin_for_pooled(self):
        self.pooled = True

