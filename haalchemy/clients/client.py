import logging
import os
import sys
import uuid

from zkcluster.client import Client
from zkcluster.util import event


from .. import service

log = logging.getLogger(__name__)


class HAClient(object):
    def __init__(
            self, username, password, hosts, clustername, appname):
        self.username = username
        self.password = password
        self.hosts = hosts
        self.clustername = clustername
        self.appname = appname
        self.zk_client = None

    def connect_persistent(self):
        if self.zk_client is not None:
            self.zk_client.close()
            self.zk_client = None

        self.client_id = str(uuid.uuid4())
        self.pid = os.getpid()
        self.processname = sys.argv[0]  # TODO: pass this in

        self.zk_client = Client.from_multi_host_port(
            self.username, self.password, self.hosts
        )
        self.zk_client.speak_rpc(service.rpc_reg)

        event.event_listen(
            self.zk_client, "client_connected", self._client_connected)
        event.event_listen(
            self.zk_client, "client_disconnected", self._client_disconnected)
        self.rpc_service = self.zk_client.rpc_service
        self.zk_client.connect()
#        self.zk_client.connect_persistent(
#            status_fn=self._connect_status, notify_every=30)

    def _connect_status(self, connected, disconnect_error, trying_for):
        if connected:
            log.info("Connected!")
        else:
            log.info(
                "Not connected (%s) trying for %s sec",
                disconnect_error, trying_for)

    def _client_connected(self, client):
        service.Identify(
            self.client_id, self.pid, self.processname,
            self.clustername, self.appname).send(self.rpc_service)
        # TODO: if this is a reconnect, send along our current slots
        # and other stats

    def _client_disconnected(self, client, unexpected, message):
        log.info("Client disconnected: %s", message)

    def get_slots(self):
        return service.GetSlots(self.client_id).send(
            self.rpc_service)
