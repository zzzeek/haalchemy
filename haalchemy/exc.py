class HAException(Exception):
    pass


class ConfigurationException(HAException):
    pass


class CantConnectException(HAException):
    pass


class BackendException(HAException):
    pass


class ConnectionFailed(BackendException):
    pass


class OperationFailed(BackendException):
    pass


class NoHostsAvailableException(HAException):
    pass




